﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEREBOROD
{
	public abstract partial class Skill
	{
		public enum SkillTargetType //Именно цель на клиенте, сама способность может например воздействовать на всех союзников
		{
			БезЦели,
			СоюзникИлиПротивник,
			Союзник,
			СоюзникБезТекущегоГероя,
			Противник
		}


		public static Random Random = new Random();

		public SkillTargetType TargetType;

		public int MaxSelectedTargets = 1;

		[NonSerialized]
		public List<GHero> Targets = new List<GHero>();

		[NonSerialized]
		public GHero Owner;

		public GBattle Battle
		{
			get { return GlobalEnvironment.AllBattles.FirstOrDefault( f => f.Id == Owner.BattleId ); }
		}


		public abstract void Action ( List<GHero> targets );

		public virtual void ContinueAction ( object data ) { }
		//public bool ContinueActionEnabled; //this param used in GValue

		public string Description;
		public string Name;

		public int Level;

		public int Id; //Конкретно для каждого класса
		public string ForClass; //Для какого класса
		public MHero.ТипГероя Class;

		public int M = 1; //Стоимость магической энергии
		public int O = 1; //Стоимость о.д.
		public int P; //Перезарядка в этапах
		public int Ptimer;
		public int D; //Длительность в этапах
		public int Mr; //Минимальный раунд
		public int R = 1;

		public void ChangeParamsToLevel ( int level )
		{
			Level = level;
			ChangeParams();
		}


		public abstract void ChangeParams ();


		protected Skill ()
		{
			ChangeParamsToLevel( 1 );
		}


		public SkillType Type;


		public enum SkillType
		{
			ФизическаяАтака,
			ФизическийУрон,
			МагическаяАтака,
			МагическийУрон,
			ЧистыйУрон,
			ФизическаяСпособность,
			МагическаяСпособность
		}

		public T ОтУровня<T> ( params T[] numbers )
		{
			return numbers[ Level - 1 ];
		}




		public T ОснованоНаПараметре<T> ( int total, params GParamValue<T>[] values )
		{
			T modifier = default( T ); //30%-60% основано на удаче (45 90 135)
			foreach ( var gParamValue in values.Reverse() )
			{
				if ( total >= gParamValue.ChangeValue )
				{
					modifier = gParamValue.ValueForSet;
					break;
				}
			}
			return modifier;
		}

		public T ОснованоНаПараметре<T> ( GParam param, params GParamValue<T>[] values )
		{
			T modifier = default( T ); //30%-60% основано на удаче (45 90 135)
			foreach ( var gParamValue in values.Reverse() )
			{
				if ( param.TotalValue >= gParamValue.ChangeValue )
				{
					modifier = gParamValue.ValueForSet;
					break;
				}
			}
			return modifier;
		}

		public static void DoEventActions ( string eventName, object eventParams, GBattle currentBattle )
		{
			foreach ( var hero in currentBattle.Heroes )
			{
				foreach ( var a in hero.PassiveAbilities.Where( w => w.HasEvent( eventName ) ) )
				{
					a.Apply( eventName, eventParams, currentBattle );
				}
			}
		}
	}

	public class GParamValue<T>
	{
		public T ValueForSet;
		public int ChangeValue;
		public GParamValue ( int changeValue, T valueForSet )
		{
			ChangeValue = changeValue;
			ValueForSet = valueForSet;
		}
	}
}