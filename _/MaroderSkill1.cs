using SEREBOROD;


public class Skill1 : Skill
		{
			public Skill1 ()
			{
				Name = "Удар мародёра";
				Description = @"
			Наносит 5/10/15 ед. урона + 10% от силы. В случае если удар завершиться смертью цели, то мародёр получит за бой золота на 10% больше.
		";
				Id = 1;
			}

			public override void Action ()
			{
			Logic_DoDamage(Owner, Targets[0], SkillType.ФизическаяАтака, Урон + Owner.Сила.TotalValue * 0.10);
		
			}

			float Урон2;
int Урон;
public override void ChangeParams () {M = 4;
O = 1;
P = 1;
Урон = ОтУровня(5, 10, 15);
Type = SkillType.ФизическаяАтака;
TargetType = SkillTargetType.Противник;
Урон2 = ОснованоНаПараметре(Owner.Ловкость.TotalValue, new[] {new GParamValue <float>( 0, 0.3f ),new GParamValue <float>( 25, 0.4f ),new GParamValue <float>( 60, 0.5f ),new GParamValue <float>( 80, 0.6f )});}
		}