﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEREBOROD
{
	[Obsolete]
	public class GHeroOLD
	{
		public void RefreshFromDB ( MHero hero = null )
		{
			GHeroOLD h = this;
			if ( string.IsNullOrWhiteSpace( h.Id ) )
				return;

			if ( hero == null )
				hero = MHero.GetHeroById( h.Id );

			h.Name = hero.Имя;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Сила ).HardValue = hero.Сила;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Ловкость ).HardValue = hero.Ловкость;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Интеллект ).HardValue = hero.Интеллект;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Удача ).HardValue = hero.Удача;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Выносливость ).HardValue = hero.Выносливость;
			h.Level = hero.Уровень;
			h.AvailablePoints = hero.СвободныеПараметры;
			h.Id = hero.Идентификатор;
			h.Experience = new Exp( hero );
		}

		public class Exp
		{
			public short Current;
			public short ForNextLevel;

			public short PCurrent
			{
				get
				{
					return ( short )Math.Ceiling( ( double )Current * 150 / ForNextLevel );
				}
			}

			public Exp ( MHero h )
			{
				if ( h.Уровень != 20 )
				{
					Current = h.Опыт;
					ForNextLevel = MHero.Experience[ h.Уровень - 1 ];
				}
				else
				{
					Current = MHero.Experience[ 18 ];
					ForNextLevel = MHero.Experience[ 18 ];
				}
			}
		}

		public byte GetSkillLevel ( byte id )
		{
			return 1;
		}

		public static GHeroOLD FromMHero ( MHero hero )
		{
			GHeroOLD h = new GHeroOLD();
			h.Name = hero.Имя;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Сила ).HardValue = hero.Сила;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Ловкость ).HardValue = hero.Ловкость;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Интеллект ).HardValue = hero.Интеллект;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Удача ).HardValue = hero.Удача;
			h.Params.FirstOrDefault( f => f.Type == GParam.ParamType.Выносливость ).HardValue = hero.Выносливость;
			h.Level = hero.Уровень;
			h.AvailablePoints = hero.СвободныеПараметры;
			h.FreeSkills = hero.СвободныеСпособности;
			h.PGames = hero.ИгрНаПассивныеСпособности;
			h.Class = ( int )hero.Тип;
			h.Id = hero.Идентификатор;
			h.Experience = new Exp( hero );

			if ( hero.ПассивныеСпособности != null )
				foreach ( var paid in hero.ПассивныеСпособности )
				{
					var np = GPassiveAbility.GetById( paid.Идентификатор );
					np.Level = paid.Уровень;
					//np.Owner = h;
					h.PassiveAbilities.Add( np );
					if ( np.HasEvent( "ПриВходе" ) )
						np.Apply( "ПриВходе", new ПриВходе { Персонаж = h } );
				}

			foreach ( var aa in hero.АктивныеСпособности )
			{
				Skill skill;
				switch ( aa.Класс )
				{
					case MHero.ТипГероя.Гладиатор:
						skill = Gladiator.GetSkill( aa.Идентификатор );
						skill.Level = aa.Уровень;
						//skill.Owner = h;
						skill.ForClass = GetClassMnemonic( aa.Класс );
						skill.Class = aa.Класс;
						if ( skill.Level > 0 )
							h.ActiveAbilities.Add( skill );
						else
							h.ZeroAbilities.Add( skill );
						break;
					case MHero.ТипГероя.Мародёр:
						skill = Maroder.GetSkill( aa.Идентификатор );
						skill.Level = aa.Уровень;
						skill.Owner = h;
						skill.ForClass = GetClassMnemonic( aa.Класс );
						skill.Class = aa.Класс;
						if ( skill.Level > 0 )
							h.ActiveAbilities.Add( skill );
						else
							h.ZeroAbilities.Add( skill );
						break;
				}
			}

			return h;
		}

		public static string GetClassMnemonic ( MHero.ТипГероя тип )
		{
			switch ( тип )
			{
				case MHero.ТипГероя.Гладиатор:
					return "Gladiator";
				case MHero.ТипГероя.Мародёр:
					return "Maroder";
				default:
					return "Unknown";
			}
		}

		public bool Alive;

		public Exp Experience;

		public string Id;
		public static IEnumerable<GHeroOLD> GetHeroes ( IEnumerable<string> heroNames )
		{
			if ( heroNames != null )
				return heroNames.Select( name => GHeroOLD.AllHeroes.FirstOrDefault( f => f.Name == name ) ).Where( hero => hero != null );
			else
				return new List<GHeroOLD>();
		}


		public short OD = 3;

		[NonSerialized]
		public static List<GHeroOLD> AllHeroes = new List<GHeroOLD>();
		public string Name;
		public string VisualName { get { return "<span class='hero'>" + Name + "</span>"; } }
		public int Class;

		public bool IsBattleBegin
		{
			get
			{
				if ( Battle != null )
					return Battle.CurrentRound != 0;
				else
					return false;
			}
		}

		public int BattleId
		{
			get
			{
				if ( Battle != null )
					return Battle.Id;
				else
					return -1;
			}
		}

		[NonSerialized]
		public GBattle Battle;

		public List<GParam> Params = new List<GParam>();


		public float KHealth
		{
			get
			{
				var pa = PassiveAbilities.FirstOrDefault( f => f.Id == 35 );
				if ( pa != null )
				{
					if ( pa.Level == 2 )
						return 1.97f;
					else if ( pa.Level == 3 )
						return 2.4f;
				}
				return 1.54f;
			}
		}

		public int Health;
		public int MaxHealth
		{
			get { return Convert.ToInt32( Params.First( f => f.Type == GParam.ParamType.Выносливость ).TotalValue * KHealth ); }
		}

		public float KEnergy
		{
			get
			{
				var pa = PassiveAbilities.FirstOrDefault( f => f.Id == 36 );
				if ( pa != null )
				{
					if ( pa.Level == 2 )
						return 1.97f;
					else if ( pa.Level == 3 )
						return 2.4f;
				}
				return 1.54f;
			}
		}

		public int Energy;
		public int MaxEnergy
		{
			get { return Convert.ToInt32( Params.First( f => f.Type == GParam.ParamType.Интеллект ).TotalValue * KEnergy ); }
		}

		public int Pdef; //в процентах
		public int Mdef; //в процентах

		public bool Side; //false - Горчви, true - Зрагоны

		public int AvailablePoints = 5;
		public int FreeSkills = 0;
		public int PGames = 0;

		//[NonSerialized]
		//public HeroType HeroType;

		public MHero.ТипГероя HeroType;

		public List<GPassiveAbility> PassiveAbilities = new List<GPassiveAbility>();
		public List<Skill> ActiveAbilities = new List<Skill>();
		public List<Skill> ZeroAbilities = new List<Skill>();

		public IEnumerable<Skill> AllSkills
		{
			get
			{
				foreach ( var skill in ActiveAbilities )
				{
					yield return skill;
				}

				foreach ( var skill in ZeroAbilities )
				{
					yield return skill;
				}
			}
		}

		public GHeroOLD ()
		{
			Params.Add( new GParam( GParam.ParamType.Сила, new GValue { Type = GValue.ValueType.Параметр, Value = 20 } ) );
			Params.Add( new GParam( GParam.ParamType.Ловкость, new GValue { Type = GValue.ValueType.Параметр, Value = 20 } ) );
			Params.Add( new GParam( GParam.ParamType.Интеллект, new GValue { Type = GValue.ValueType.Параметр, Value = 20 } ) );
			Params.Add( new GParam( GParam.ParamType.Удача, new GValue { Type = GValue.ValueType.Параметр, Value = 20 } ) );
			Params.Add( new GParam( GParam.ParamType.Выносливость, new GValue { Type = GValue.ValueType.Параметр, Value = 20 } ) );
			Params.Add( new GParam( GParam.ParamType.ФизическаяЗащита, new GValue { Type = GValue.ValueType.Параметр, Value = 0 } ) );
			Params.Add( new GParam( GParam.ParamType.МагическаяЗащита, new GValue { Type = GValue.ValueType.Параметр, Value = 0 } ) );
			Params.Add( new GParam( GParam.ParamType.ПовышениеУрона ) ); //только в процентах значениях
			Params.Add( new GParam( GParam.ParamType.ПовышениеЗащиты ) ); //только в процентах значениях
			Params.Add( new GParam( GParam.ParamType.Баффы ) );
		}


		public string GetClassName ()
		{
			switch ( Class )
			{
				case 1:
					return "Гладиатор";
				case 2:
					return "Мародёр";
				default:
					return "Неизвестно";
			}
		}

		public int Level = 1;

		public GHeroView View ()
		{
			GHeroView gh = new GHeroView();
			gh.Сила = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Сила ).TotalValue;
			gh.Ловкость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Ловкость ).TotalValue;
			gh.Интеллект = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Интеллект ).TotalValue;
			gh.Удача = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Удача ).TotalValue;
			gh.Выносливость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Выносливость ).TotalValue;

			gh.ФизическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.ФизическаяЗащита ).TotalValue;
			gh.МагическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.МагическаяЗащита ).TotalValue;

			gh.Класс = GetClassName();
			gh.Персонаж = Name;
			gh.СвободныеПараметры = AvailablePoints;
			return gh;

			//GHeroView gh = new GHeroView();
			//gh.Сила = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Сила ).HardValue;
			//gh.Ловкость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Ловкость ).HardValue;
			//gh.Интеллект = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Интеллект ).HardValue;
			//gh.Удача = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Удача ).HardValue;
			//gh.Выносливость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Выносливость ).HardValue;

			//gh.ФизическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.ФизическаяЗащита ).HardValue;
			//gh.МагическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.МагическаяЗащита ).HardValue;

			//gh.Класс = GetClassName();
			//gh.Персонаж = Name;
			//gh.СвободныеПараметры = AvailablePoints;
			//return gh;

			//GHeroView gh = new GHeroView();
			//gh.Сила = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Сила ).HardValue;
			//gh.Ловкость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Ловкость ).HardValue;
			//gh.Интеллект = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Интеллект ).HardValue;
			//gh.Удача = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Удача ).HardValue;
			//gh.Выносливость = Params.FirstOrDefault( f => f.Type == GParam.ParamType.Выносливость ).HardValue;

			//gh.ФизическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.ФизическаяЗащита ).HardValue;
			//gh.МагическаяЗащита = Params.FirstOrDefault( f => f.Type == GParam.ParamType.МагическаяЗащита ).HardValue;

			//gh.Класс = GetClassName();
			//gh.Персонаж = Name;
			//gh.СвободныеПараметры = AvailablePoints;
			//return gh;

		}


		public GParam Сила
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Сила ); }
		}
		public GParam Ловкость
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Ловкость ); }
		}
		public GParam Интеллект
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Интеллект ); }
		}
		public GParam Удача
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Удача ); }
		}
		public GParam Выносливость
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Выносливость ); }
		}
		public GParam ФизическаяЗащита
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.ФизическаяЗащита ); }
		}
		public GParam МагическаяЗащита
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.МагическаяЗащита ); }
		}
		public GParam ПовышениеУрона
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.ПовышениеУрона ); }
		}
		public GParam ПовышениеЗащиты
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.ПовышениеЗащиты ); }
		}
		public GParam Баффы
		{
			get { return Params.First( f => f.Type == SEREBOROD.GParam.ParamType.Баффы ); }
		}
		[NonSerialized]
		public List<GClientSkillParam> Slots; //Этапы. Для способностей.
	}

}