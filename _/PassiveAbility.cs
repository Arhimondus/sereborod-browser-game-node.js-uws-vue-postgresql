﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEREBOROD
{
	public abstract class GPassiveAbility
	{
		public byte Level;
		public byte MaxLevel = 3;
		public short Id;
		public string Name;
		public string Description;
		public bool Custom;
		public int D = 1; //For custom only

		[NonSerialized]
		public GHero Owner;

		public static GPassiveAbility FromMPassiveAbility ( GHero hero, MPassiveAbility rawa )
		{
			var a = GetById( rawa.Идентификатор );
			a.Level = rawa.Уровень;
			a.Owner = hero;
			return a;
		}

		/// <summary>
		/// Список событий: 
		/// 
		/// ДоСмертиПерсонажа
		/// ПослеСмертиПерсонажа
		/// 
		/// ДоНанесенияУронаПерсонажу
		/// ДоНанесенияФизическогоУронаПерсонажу
		/// ДоНанесенияМагическогоУронаПерсонажу
		/// ДоНанесенияЧистогоУронаПерсонажу
		/// 
		/// ПослеНанесенияУронаПерсонажу
		/// ПослеНанесенияФизическогоУронаПерсонажу
		/// ПослеНанесенияМагическогоУронаПерсонажу
		/// ПослеНанесенияЧистогоУронаПерсонажу
		/// 
		/// ПриВходе
		/// 
		/// НачалоБоя
		/// КонецБоя
		/// 
		/// ДоНачалаРаунда
		/// ПослеНачалаРаунда
		/// 
		/// ДоКонцаРаунда
		/// ПослеКонцаРаунда
		/// 
		/// ДоНачалаЭтапа
		/// ПослеНачалаЭтапа
		/// 
		/// ДоКонцаЭтапа
		/// ПослеКонцаЭтапа
		/// </summary>
		public List<string> EventSubscribeList = new List<string>();

		public bool HasEvent ( string @event )
		{
			return EventSubscribeList.Contains( @event );
		}

		public virtual void Apply ( string eventType, object eventParams, GBattle battle = null ) { }

		public static GPassiveAbility GetById ( int id )
		{
			switch ( id )
			{
				case 1:
					return new P1();
				case 2:
					return new P2();
				case 3:
					return new P3();
				case 4:
					return new P4();
				case 5:
					return new P5();
				case 6:
					return new P6();
				case 7:
					return new P7();
				case 8:
					return new P8();
				case 9:
					return new P9();
				case 10:
					return new P10();
				case 11:
					return new P11();
				case 12:
					return new P12();
				case 13:
					return new P13();
				case 14:
					return new P14();
				case 15:
					return new P15();
				case 16:
					return new P16();
				case 17:
					return new P17();
				case 18:
					return new P18();
				case 19:
					return new P19();
				case 20:
					return new P20();
				case 21:
					return new P21();
				case 22:
					return new P22();
				case 23:
					return new P23();
				case 24:
					return new P24();
				case 25:
					return new P25();
				case 26:
					return new P26();
				case 27:
					return new P27();
				case 28:
					return new P28();
				case 29:
					return new P29();
				case 30:
					return new P30();
				case 31:
					return new P31();
				case 32:
					return new P32();
				case 33:
					return new P33();
				case 34:
					return new P34();
				case 35:
					return new P35();
				case 36:
					return new P36();
				case 37:
					return new P37();
				case 38:
					return new P38();
				case 39:
					return new P39();
				case 40:
					return new P40();
				case 41:
					return new P41();
				case 42:
					return new P42();
				case 43:
					return new P43();
				case 44:
					return new P44();
				case 45:
					return new P45();
				case 46:
					return new P46();
				case 47:
					return new P47();
				case 48:
					return new P48();
				case 49:
					return new P49();
				case 50:
					return new P50();
				case 51:
					return new P51();
				case 52:
					return new P52();
				default:
					return null;
			}
		}

		public T ОтУровня<T> ( params T[] numbers )
		{
			return numbers[ Level - 1 ];
		}

		public T ОснованоНаПараметре<T> ( int total, params GParamValue<T>[] values )
		{
			T modifier = default( T ); //30%-60% основано на удаче (45 90 135)
			foreach ( var gParamValue in values.Reverse() )
			{
				if ( total >= gParamValue.ChangeValue )
				{
					modifier = gParamValue.ValueForSet;
					break;
				}
			}
			return modifier;
		}

		public T ОснованоНаПараметре<T> ( GParam param, params GParamValue<T>[] values )
		{
			T modifier = default( T ); //30%-60% основано на удаче (45 90 135)
			foreach ( var gParamValue in values.Reverse() )
			{
				if ( param.TotalValue >= gParamValue.ChangeValue )
				{
					modifier = gParamValue.ValueForSet;
					break;
				}
			}
			return modifier;
		}

		public const int MaxCountOfPassiveAbilities = 52;
	}

	public class ПослеНанесенияУронаПерсонажу
	{
		public GHero Инициатор;
		public GHero Жертва;
		public IntegerRef Урон;

		public Skill.SkillType ТипУрона;
	}

	public class ДоНанесенияУронаПерсонажу
	{
		public GHero Инициатор;
		public GHero Жертва;

		public Skill Способность;
		public Skill.SkillType ТипУрона;
		public IntegerRef ПредполагаемыйУрон;
	}

	public class ДоСмертиПерсонажа
	{
		public GHero ПредполагаемыйУбийца;
		public GHero Жертва;

		public IntegerRef УронКоторогоДостаточноДляУбийства;
	}

	public class ПослеСмертиПерсонажа
	{
		public GHero Убийца;
		public GHero Жертва;
		public IntegerRef ПоследнийНанесённыйУрон;
	}

	public class IntegerRef
	{
		public int Value;
		public IntegerRef ()
		{
		}
		public IntegerRef ( int value )
		{
			Value = value;
		}
	}

	public class ПриВходе
	{
		public GHero Персонаж;
	}

	public class P1 : GPassiveAbility
	{
		public P1 ()
		{
			Id = 1;
			Name = "Вампиризм";
			Description = "Восстанавливает 7.5 / 15 / 22.5% от нанесённого урона физическими атаками в здоровье.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПослеНанесенияУронаПерсонажу" )
			{
				var data = ( ПослеНанесенияУронаПерсонажу )eventParams;
				if ( Owner == data.Инициатор )
				{
					var r = Skill.Logic_RestoreHealth( data.Инициатор, data.Инициатор, Skill.SkillType.ФизическаяСпособность, data.Урон.Value * ОтУровня( 0.075, 0.15, 0.225 ) );
					battle.ToLog( Owner.Name + " восстанавливает себе <span class='green'>" + r.ИтоговоеЗначение + "</span> единиц здоровья." );
				}
			}
		}
	}

	public class P2 : GPassiveAbility
	{
		public P2 ()
		{
			Id = 2;
			Name = "Критический удар";
			Description = "Вероятность 10 / 15 / 20 % нанести в два раза больше урона физическими атаками.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу" )
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( Owner == data.Инициатор && data.Способность.Type == Skill.SkillType.ФизическаяАтака )
				{
					if ( Skill.Random.Next( 101 ) <= ОтУровня( 10, 15, 20 ) )
					{
						data.ПредполагаемыйУрон.Value *= 2;
					}
				}
			}
		}
	}

	public class P3 : GPassiveAbility
	{
		public P3 ()
		{
			Id = 3;
			Name = "Дьявольский удар";
			Description = "Вероятность 2-7%(основано на Ловкости) нанести в 3 / 4 / 5 раз(а) больше урона физическими атаками.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу" )
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( Owner == data.Инициатор && data.Способность.Type == Skill.SkillType.ФизическаяАтака )
				{
					var chance = ОснованоНаПараметре( Owner.Ловкость, new[]
					{
						new GParamValue <byte>( 0, 2 ),
						new GParamValue <byte>( 30, 3 ),
						new GParamValue <byte>( 60, 4 ),
						new GParamValue <byte>( 90, 5 ),
						new GParamValue <byte>( 120, 6 ),
						new GParamValue <byte>( 150, 7 )
					} );

					if ( Skill.Random.Next( 101 ) <= chance )
					{
						data.ПредполагаемыйУрон.Value *= 2;
					}

				}
			}
		}
	}

	public class P4 : GPassiveAbility
	{
		public P4 ()
		{
			Id = 4;
			Name = "Эмпатия";
			Description = "15% урона снижается каждому из союзных персонажей и на каждые 1 / 1.75 / 2.50 ед. урона снижается 1 ед. магической энергии, в случае если магическая энергия исчерпана, то оставшийся урон полностью применяется к здоровью.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу" )
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( data.Инициатор.Side == Owner.Side && data.Инициатор != Owner ) //Если союзник
				{
					var unm = ОтУровня( 1f, 1.75f, 2.5f );

					var upem = ( int )Math.Floor( data.ПредполагаемыйУрон.Value * 0.15f );
					data.ПредполагаемыйУрон.Value -= upem;

					var removeEnergy = ( int )Math.Floor( upem / unm );
					var result = Skill.Logic_RemoveEnergy( Owner, Owner, Skill.SkillType.МагическаяСпособность, removeEnergy );
					battle.ToLog( Owner.Name + " снижает урон по игроку " + data.Жертва.Name + " на 15% и теряет " + result.ИтоговоеЗначение + " ед. магической энергии." );

					var removePoint = removeEnergy - result.ИтоговоеЗначение;
					if ( removePoint > 0 )
					{
						var result2 = Skill.Logic_DoDamage( data.Жертва, Owner, Skill.SkillType.МагическаяСпособность, data.Способность, removeEnergy );
						battle.ToLog( Owner.Name + " теряет " + result.ИтоговоеЗначение + " ед. здоровья защищая союзника" );
					}
				}
			}
		}
	}

	public class P5 : GPassiveAbility
	{
		public P5 ()
		{
			Id = 5;
			Name = "Усиленная защита";
			Description = "Снижает получаемый урон от физических атак на 8 / 16 / 24%.";
			EventSubscribeList.Add( "ПриВходе" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПриВходе" )
			{
				var data = ( ПриВходе )eventParams;
				if ( data.Персонаж != null )
				{
					data.Персонаж.ФизическаяЗащита.Value.Add( new GValue
					{
						Type = GValue.ValueType.ПассивнаяСпособность,
						NumberType = GValue.ValueNumberType.Абсолютное,
						Value = ОтУровня( 8, 16, 24 ),
						Time = -1
					} );
				}
			}
		}
	}

	public class P6 : GPassiveAbility
	{
		public P6 ()
		{
			Id = 6;
			Name = "Выносливость";
			Description = "Снижает получаемый урона от всех типов атак (кроме чистого снятия) на 6 / 10 / 14%.";
			EventSubscribeList.Add( "ПриВходе" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПриВходе" )
			{
				var data = ( ПриВходе )eventParams;
				if ( data.Персонаж != null )
				{
					var def = ОтУровня( 6, 10, 14 );
					data.Персонаж.ФизическаяЗащита.Value.Add( new GValue
					{
						Type = GValue.ValueType.ПассивнаяСпособность,
						NumberType = GValue.ValueNumberType.Абсолютное,
						Value = def,
						Time = -1
					} );

					data.Персонаж.МагическаяЗащита.Value.Add( new GValue
					{
						Type = GValue.ValueType.ПассивнаяСпособность,
						NumberType = GValue.ValueNumberType.Абсолютное,
						Value = def,
						Time = -1
					} );
				}
			}
		}
	}

	public class P7 : GPassiveAbility
	{
		public P7 ()
		{
			Id = 7;
			Name = "Магическая броня";
			Description = "Снижает получаемый урон от магических так на 9 / 18 / 27%.";
			EventSubscribeList.Add( "ПриВходе" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПриВходе" )
			{
				var data = ( ПриВходе )eventParams;
				if ( data.Персонаж != null )
				{
					data.Персонаж.МагическаяЗащита.Value.Add( new GValue
					{
						Type = GValue.ValueType.ПассивнаяСпособность,
						NumberType = GValue.ValueNumberType.Абсолютное,
						Value = ОтУровня( 9, 18, 27 ),
						Time = -1
					} );
				}
			}
		}
	}

	public class P8 : GPassiveAbility
	{
		public P8 ()
		{
			Id = 8;
			Name = "Отражение боли";
			Description = "10/20/20% от каждого нанесённого урона восстанавливает некоторым союзникам здоровье. Количество союзников 1/1/2.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПослеНанесенияУронаПерсонажу" )
			{
				var data = ( ПослеНанесенияУронаПерсонажу )eventParams;
				if ( data.Жертва == Owner )
				{
					var u = ( int )Math.Ceiling( data.Урон.Value * ОтУровня( 0.1f, 0.2f, 0.2f ) );

					var allies = battle.AliveHeroes.Where( w => w.Side == Owner.Side && w != Owner ).ToList();
					if ( allies.Count > 0 )
					{
						var hero1 = allies[ Skill.Random.Next( allies.Count ) ];
						Skill.Logic_RestoreHealth( Owner, hero1, Skill.SkillType.МагическаяСпособность, u );
						allies.Remove( hero1 );

						if ( Level == 3 && allies.Count > 0 )
						{
							var hero2 = allies[ Skill.Random.Next( allies.Count ) ];
							Skill.Logic_RestoreHealth( Owner, hero2, Skill.SkillType.МагическаяСпособность, u );
						}
					}
				}
			}
		}
	}

	public class P9 : GPassiveAbility
	{
		public P9 ()
		{
			Id = 9;
			Name = "Паралич (не реализовано)";
			Description = "Каждая физическая атака имеет 15% вероятность парализовать противника на 1 / 2 / 3 о.д.";
		}
	}

	public class P10 : GPassiveAbility
	{
		public P10 ()
		{
			Id = 10;
			Name = "Жажда смерти";
			Description = "Каждая физическая атака наносит бонусный урон каждые 20% потерянного здоровья вражеского персонажа. В начале бонус  равен 0. Каждый уровень бонуса равен 10 / 14 / 20% урона. Макс. 4.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу" )
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( data.Инициатор == Owner && data.Способность.Type == Skill.SkillType.ФизическаяАтака )
				{
					var pnt = ( int )Math.Floor( ( double )data.Жертва.Health * 100 / data.Жертва.MaxHealth );
					var bonus = 0f;
					if ( pnt < 20 )
					{
						bonus += 4 * ОтУровня( 0.1f, 0.14f, 0.2f );
					}
					else if ( pnt < 40 )
					{
						bonus += 3 * ОтУровня( 0.1f, 0.14f, 0.2f );
					}
					else if ( pnt < 60 )
					{
						bonus += 2 * ОтУровня( 0.1f, 0.14f, 0.2f );
					}
					else if ( pnt < 80 )
					{
						bonus += ОтУровня( 0.1f, 0.14f, 0.2f );
					}

					battle.ToLog( Name + ": " + "Жажда смерти увеличивает урон по персонажу " + data.Жертва.Name + " на эту атаку." );
					data.ПредполагаемыйУрон.Value += ( int )Math.Floor( data.ПредполагаемыйУрон.Value * bonus );
				}
			}
		}
	}

	public class P11 : GPassiveAbility
	{
		public P11 ()
		{
			Id = 11;
			Name = "Предсказание смерти";
			Description = "Один раз за бой в случае если суммарный урон был больше чем текущее количество ед. здоровья, то вы не умираете и восстанавливаете 1 / 5 / 13 % от максимального здоровья.";
			EventSubscribeList.Add( "ДоКонцаЭтапа" );
		}

		public bool Already = false;
		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоКонцаЭтапа" )
			{
				if ( !Already )
				{
					if ( Owner.Health <= 0 )
					{
						Owner.Health = ( int )Math.Floor( Owner.MaxHealth * ОтУровня( 0.03f, 0.09f, 0.15f ) );
						Already = true;
						battle.ToLog( Name + ":" + Owner.Name + " предсказывает свою смерть и восстанавливается себе немного здоровья" );
					}
				}
			}
		}
	}

	public class P12 : GPassiveAbility
	{
		public P12 ()
		{
			Id = 12;
			Name = "Добивание";
			Description = "Влияет на физические атаки. В случае если у противника меньше 5/10/15% здоровья, то тогда дополнительно нанесётся чистый урон в размере текущего здоровья вражеского персонажа. Этот модификатор срабатывает перед нанесением урона.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу" )
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( data.Инициатор == Owner && data.Способность.Type == Skill.SkillType.ФизическаяАтака )
				{
					if ( data.Жертва.Health < data.Жертва.MaxHealth * ОтУровня( 0.05f, 0.1f, 0.15f ) )
					{
						data.ПредполагаемыйУрон.Value += data.Жертва.Health;
						battle.ToLog( Name + "(" + data.Инициатор.Name + "=>" + data.Жертва.Name + ": Добивание добавляет бонус в " + data.Жертва.Health + " ед. урона" );
					}
				}
			}
		}
	}

	public class P13 : GPassiveAbility
	{
		public P13 ()
		{
			Id = 13;
			Name = "Магический критический удар";
			Description = "Вероятность 9 / 14 / 19 % нанести двойной урон магическими атаками.";
			EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ДоНанесенияУронаПерсонажу")
			{
				var data = ( ДоНанесенияУронаПерсонажу )eventParams;
				if ( data.Инициатор == Owner && data.Способность.Type == Skill.SkillType.МагическаяАтака && Skill.Random.Next( 101 ) <= ОтУровня( 9, 14, 19 ) )
				{
					data.ПредполагаемыйУрон.Value *= 2;
					battle.ToLog( Name + ": " + Owner.Name + " увеличивает магический урон на данную атаку" );
				}
			}
		}
	}

	public class P14 : GPassiveAbility
	{
		public P14 ()
		{
			Id = 14;
			Name = "Цепь магий";
			Description = "15 / 25 / 35 % от нанесённого урона магическими атаками равномерно распределяется между остальными вражескими персонажами. (Это бонусный урон)";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}

		public override void Apply ( string eventType, object eventParams, GBattle battle = null )
		{
			if ( eventType == "ПослеНанесенияУронаПерсонажу" )
			{
				var data = ( ПослеНанесенияУронаПерсонажу )eventParams;
				if ( Owner == data.Инициатор )
				{
					var enemies = battle.AliveHeroes.Where( w => w.Side != Owner.Side ).ToList();
					if ( enemies.Count > 0 )
					{
						var uron = data.Урон.Value * ОтУровня( 0.15f, 0.25f, 0.35f ) / enemies.Count;
						foreach ( var hero in enemies )
						{
							var r = Skill.Logic_DoDamage( Owner, hero, Skill.SkillType.МагическийУрон, null, uron );
							if(r.Успешно){
								battle.ToLog( Name + "(" + Owner.Name + "=>" + hero.Name + "): Наносит <span class='red'>" + r.ИтоговыйУрон + "</span> ед. урона." );
							}
						}
					}
				}
			}
		}
	}

	public class P15 : GPassiveAbility
	{
		public P15 ()
		{
			Id = 15;
			Name = "Ловушка";
			Description = "При физических атаках по вам противник может попасть в ловушку с вероятностью 10%, которая нанесёт {0}*Сила ед. урона и парализует на {1} о.д. При этом сама атака не наносится. (Возможно будут исключения).";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P16 : GPassiveAbility
	{
		public P16 ()
		{
			Id = 16;
			Name = "Молния-помощник";
			Description = "Есть вероятность 13% при использовании магических способностей произнести способность молния, которая нанесёт случайному противнику урон в размере Интеллект*{0}.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P17 : GPassiveAbility
	{
		public P17 ()
		{
			Id = 17;
			Name = "Фамильяр";
			Description = "Когда один из союзников умирает, то на помощь приходит фамильяр, который имеет Интеллект*{0} ед. здоровья и который имеет возможность атаковать случайную цель физической атакой в размере Интеллект+Сила. Максимально в бою может быть только один фамильяр от данной способности. Длится 5 раундов.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P18 : GPassiveAbility
	{
		public P18 ()
		{
			Id = 18;
			Name = "Безумие";
			Description = "Все физические атаки наносят на {0}% больше урона, однако весь получаемый урон увеличивается на 10%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P19 : GPassiveAbility
	{
		public P19 ()
		{
			Id = 19;
			Name = "Оплакивание";
			Description = "За каждого второго умершего персонажа герой получает бонус к Интеллекту в размере {0}% и Силе в размере {1}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P20 : GPassiveAbility
	{
		public P20 ()
		{
			Id = 20;
			Name = "Жажда боли";
			Description = "Каждые 20% потерянного здоровья, получаемый урон персонажем возрастает на 5% и Сила на {0}%. Максимально стаков 4.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P21 : GPassiveAbility
	{
		public P21 ()
		{
			Id = 21;
			Name = "Абсолютное сумасшедствие";
			Description = "Каждая физическая атака снимает с вашего персонажа {0}% от текущего здоровья и наносит его дополнительно в виде урона.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P22 : GPassiveAbility
	{
		public P22 ()
		{
			Id = 22;
			Name = "Магическое безумие";
			Description = "Каждый % потерянного здоровья увеличивает урон от магических атак на {0}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P23 : GPassiveAbility
	{
		public P23 ()
		{
			Id = 23;
			Name = "Озарение";
			Description = "Каждый раунд проверяется вероятность 15%, если эта вероятность срабатывает, то персонаж получает бонус Интеллект в размере {0}% на 3 раунда. Срабатывает только один раз за бой.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P24 : GPassiveAbility
	{
		public P24 ()
		{
			Id = 24;
			Name = "Второе дыхание";
			Description = "Спустя 2 раунда после вашей смерти есть вероятность 40% каждый раунд воскреситься с 15% здоровья и бонусной Силой в размере {0}%. Срабатывает только один раз за бой. Действует 3 раунда..";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P25 : GPassiveAbility
	{
		public P25 ()
		{
			Id = 25;
			Name = "Чёрный ход";
			Description = "Если ваш персонаж остаётся один, то начиная через {0} раунд появляется возможность сбежать с поля боя (при условии, что уровень здоровья менее 25%).";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P26 : GPassiveAbility
	{
		public P26 ()
		{
			Id = 26;
			Name = "Контратака";
			Description = "Вероятность {0}%. Отвечает на физическую атаку атакой равной нанесённому урона в размере {0}.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P27 : GPassiveAbility
	{
		public P27 ()
		{
			Id = 27;
			Name = "Уклонение";
			Description = "Вероятность 3-10(основано на Ловкости) + {0}% уклониться от физических атак. (Возможны исключения).";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P28 : GPassiveAbility
	{
		public P28 ()
		{
			Id = 28;
			Name = "Сглаз";
			Description = "Противник с вероятностью {0} при исп. направленных атак может перепутать цель и атаковать другого противника или союзника.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P29 : GPassiveAbility
	{
		public P29 ()
		{
			Id = 29;
			Name = "Регенерация";
			Description = "Каждый раунд вы восстанавливаете {0}% от макс.здоровья.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P30 : GPassiveAbility
	{
		public P30 ()
		{
			Id = 30;
			Name = "Вечное проклятие";
			Description = "Вы со 100% вероятностью не можете умереть в первые 4 раунда, а затем эта вероятность каждый раунд уменьшается на 25%, у вас всегда остаётся 1 ед. здоровья, но наносимый урон вами снижен на {0}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P31 : GPassiveAbility
	{
		public P31 ()
		{
			Id = 31;
			Name = "Бессмертие";
			Description = "Вы не можете умереть. Вы не можете убить. У противника после ваших действий всегда остаётся 1 ед. здоровья так же как и у вас. Вы не влияете на результат боя (т.е. если остаётся хотя бы один противник и вы, то считается, что вы проиграли).";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P32 : GPassiveAbility
	{
		public P32 ()
		{
			Id = 32;
			Name = "Подарок судьбы";
			Description = "Вы получаете бонус к Интеллекту, Ловкости и Силе в размере 20%, но каждый раунд в бою появляется 7% вероятность умереть. За каждый подарок нужно платить.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P33 : GPassiveAbility
	{
		public P33 ()
		{
			Id = 33;
			Name = "Длань богов";
			Description = "Вы получаете бонус к Ловкости и Силе в размере {0}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P34 : GPassiveAbility
	{
		public P34 ()
		{
			Id = 34;
			Name = "Безмозглый";
			Description = "Интеллект снижается на {0}%. Сила увеличивается на {1}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	
	public class P35 : GPassiveAbility
	{
		public P35 ()
		{
			Id = 35;
			Name = "Здоровья";
			Description = "Увеличивает на каждую единицу Выносливости здоровье на 1.54 / 1.97 / 2.40 ед.";
		}
	}

	public class P36 : GPassiveAbility
	{
		public P36 ()
		{
			Id = 35;
			Name = "Магия";
			Description = "Увеличивает на каждую единицу Интеллекта магическую энергию на 1.54 / 1.97 / 2.40 ед.";
		}
	}

	public class P37 : GPassiveAbility
	{
		public P37 ()
		{
			Id = 37;
			Name = "Иммунитет к магическому урону";
			Description = "Снижает на 55% весь урон от магических атак, но увеличивается получаемый урон от физических атак на {0}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P38 : GPassiveAbility
	{
		public P38 ()
		{
			Id = 38;
			Name = "Железный панцирь";
			Description = "Снижает на 45% весь урон от физических атак, но увеличивается получаемый урон от магических атак на {0}%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P39 : GPassiveAbility
	{
		public P39 ()
		{
			Id = 39;
			Name = "Любвиобильность";
			Description = "За каждого женского персонажа в бою данный персонаж получает 5% Силы.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P40 : GPassiveAbility
	{
		public P40 ()
		{
			Id = 40;
			Name = "Жажда жизни";
			Description = "За каждого живого персонажа (кроме этого) даётся бонус 5% к Силе и Выносливости, а за каждую смерть снимается 10%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P41 : GPassiveAbility
	{
		public P41 ()
		{
			Id = 41;
			Name = "Зверский удар";
			Description = "Зверский удар, отрывающий конечности. Вероятность 4% при исп. физических атак. Снимает все положительные эффекты и наносит {0}% урона от макс. здоровья.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P42 : GPassiveAbility
	{
		public P42 ()
		{
			Id = 42;
			Name = "Возмездие";
			Description = "Даёт полную неуязвимость на 2 раунда, если в пред. раунде умерло 2 или более союзников.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P43 : GPassiveAbility
	{
		public P43 ()
		{
			Id = 43;
			Name = "Месть";
			Description = "За каждую смерть союзника персонаж получает {0}% бонуса к Силе.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P44 : GPassiveAbility
	{
		public P44 ()
		{
			Id = 44;
			Name = "Великий ловкач";
			Description = "Даёт пассивный бонус {0}% к Ловкости.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P45 : GPassiveAbility
	{
		public P45 ()
		{
			Id = 45;
			Name = "Мировой разум";
			Description = "Даёт пассивный бонус {0}% к Интеллекту.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P46 : GPassiveAbility
	{
		public P46 ()
		{
			Id = 46;
			Name = "Истинный силач";
			Description = "Даёт пассивный бонус {0}% к Силе, но снижает Ловкость на 25%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P47 : GPassiveAbility
	{
		public P47 ()
		{
			Id = 47;
			Name = "Сверхвыносливый";
			Description = "Даёт пассивный бонус {0}% к Выносливости.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P48 : GPassiveAbility
	{
		public P48 ()
		{
			Id = 48;
			Name = "Математика зла";
			Description = "Каждый раз когда количество персонажей будет нечётным числом, то все персонажи кроме текущего делятся попалами (при этом противники в начале списка идут) и у первой половины отнимается {0}% всех параметров и передаёт второй половине.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P49 : GPassiveAbility
	{
		public P49 ()
		{
			Id = 49;
			Name = "Сбор крови (Донорство)";
			Description = "Каждый раунд у всех союзников забирается 3% от максимального здоровья и передаётся текущему персонажу в виде здоровья.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P50 : GPassiveAbility
	{
		public P50 ()
		{
			Id = 50;
			Name = "Ускоренная магическая регенерация";
			Description = "Каждый раунд вы восстанавливаете {0}% от макс.магич.энергии.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P51 : GPassiveAbility
	{
		public P51 ()
		{
			Id = 51;
			Name = "Удачный день";
			Description = "Если вы выигрываете подряд 6-3 боя (основано на удаче 0/40/60/80/100), то тогда ваш персонаж получается бонус 10% к урону до конца дня (серверного). Кроме этого персонаж будет получать бонус золото за каждый бой (обсуждаемо).";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P52 : GPassiveAbility
	{
		public P52 ()
		{
			Id = 52;
			Name = "Ночная жизнь";
			Description = "Ночью с 22:00 до 4:00 персонаж получается 15% к удаче и 5% к интеллекту";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P53 : GPassiveAbility
	{
		public P53 ()
		{
			Id = 53;
			Name = "Заблудившийся";
			Description = "Понижает выносливость и удачу персонажа на 10%, а у всех противников удачу на 25% на первые два раунда.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P54 : GPassiveAbility
	{
		public P54 ()
		{
			Id = 54;
			Name = "Меценат";
			Description = "40% получаемого золота в бою равномерно распределяется между всеми союзниками, при этом сам персонаж получает 10% к удаче.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P55 : GPassiveAbility
	{
		public P55 ()
		{
			Id = 55;
			Name = "Исскусный боец";
			Description = "Есть вероятность 10-35% (основано на Удаче) нанести в бою бонус 5-30 (основано на Силе) единиц урона физическими атаками.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P56 : GPassiveAbility
	{
		public P56 ()
		{
			Id = 56;
			Name = "Сильный разум";
			Description = "Есть вероятность 10-35%(основано на Удаче) нанести бонус {0}% от Силы магическими атаками, работает только в случае если у персонажа не менее 65% магической энергии.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
	public class P57 : GPassiveAbility
	{
		public P57 ()
		{
			Id = 57;
			Name = "Судный день";
			Description = "Перед началом боя выбирается число между 3 и 13 (при это этот раунд сообщается), когда наступит этот раунд все персонажи получают дебафф на -20% силы, интеллекта, ловкости, удачи на 2 раунда и наносится 15% чистого урона от текущего здоровья.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}

	public class P58 : GPassiveAbility
	{
		public P58 ()
		{
			Id = 58;
			Name = "Бутылка пива/рома";
			Description = "Всем противникам и союзникам выдаётся по бутылочке роме, все начинают промахиваться до конца боя с вероятностью 7%.";
			EventSubscribeList.Add( "ПослеНанесенияУронаПерсонажу" );
		}
	}
}