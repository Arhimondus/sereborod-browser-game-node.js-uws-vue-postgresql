﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;


namespace SEREBOROD
{
	public class Maroder : HeroType
	{
		public GHero Owner;
		public Maroder ( GHero owner )
		{
			Owner = owner;
			Skills = new List<Skill>( new Skill[] { new Skill1(), new Skill2(), new Skill3(), new Skill4(), new Skill5(), new Skill6(), new Skill7(), new Skill8(), new Skill9(), new Skill10() } );
			foreach ( var skill in Skills )
			{
				skill.Owner = owner;
				skill.ForClass = "Maroder";
				skill.Level = 0;
			}
		}

		public static Skill GetSkill ( int id )
		{
			switch ( id )
			{
				case 1:
					return new Skill1();
				case 2:
					return new Skill2();
				case 3:
					return new Skill3();
				case 4:
					return new Skill4();
				case 5:
					return new Skill5();
				case 6:
					return new Skill6();
				case 7:
					return new Skill7();
				case 8:
					return new Skill8();
				case 9:
					return new Skill9();
				case 10:
					return new Skill10();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}


		public class Skill1 : Skill
		{
			public Skill1 ()
			{
				Name = "Удар мародёра";
				Description = "Наносит 5/10/15 ед. урона + 10% от силы. В случае если удар завершиться смертью цели, то мародёр получит за бой золота на 10% больше.";
				Id = 1;
			}

			public override void Action ( List<GHero> targets )
			{
				var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическаяАтака, this, Uron + Owner.Сила.TotalValue * 0.10 );
				if ( r.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + targets[ 0 ].VisualName + " получает урон в размере <span class='red'>" + r.ИтоговыйУрон + "</span>." );
				}
			}

			public int Uron;

			public override void ChangeParams ()
			{
				M = 4;
				O = 1;
				P = 0;
				Uron = ОтУровня( 5, 10, 15 );
				Type = SkillType.ФизическаяАтака;
				TargetType = SkillTargetType.Противник;
			}
		}


		public class Skill2 : Skill
		{
			public Skill2 ()
			{
				Name = "Стремительная атака";
				Description = "Наносит урон в размере 20/35/50% от силы, в случае если у противника остаётся менее 30-60% (основано на Удаче) здоровья, то парализует цель на 1 О.";
				Id = 2;
			}

			public override void Action ( List<GHero> targets )
			{
				var target = targets[ 0 ];

				var r = Logic_DoDamage( Owner, target, SkillType.ФизическаяАтака, this, Owner.Сила.TotalValue * Uron );
				if ( r.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + Owner.VisualName + " наносит урон персонажу " + targets[ 0 ].VisualName + " в размере <span class='red'>" + r.ИтоговыйУрон + "</span> и парализует его на 1 ОД." );

					//float minHealthModifier; //30%-60% основано на удаче (45 90 135)
					//if(Owner.Удача.TotalValue >= 135){
					//    minHealthModifier = 0.60f;
					//}else if(Owner.Удача.TotalValue >= 90){
					//    minHealthModifier = 0.50f;
					//}else if(Owner.Удача.TotalValue >= 45){
					//    minHealthModifier = 0.40f;
					//}else{
					//    minHealthModifier = 0.30f;
					//}

					var minHealthModifier = ОснованоНаПараметре( Owner.Удача, new[]
					{
						new GParamValue <float>( 0, 0.3f ),
						new GParamValue <float>( 45, 0.4f ),
						new GParamValue <float>( 90, 0.5f ),
						new GParamValue <float>( 135, 0.6f )
					} );

					if ( target.Health <= target.MaxHealth * minHealthModifier )
					{
						target.OD--; //Не забыть восстановить ОД где-нибудь
					}
				}
			}

			public float Uron;

			public override void ChangeParams ()
			{
				M = 6;
				O = 2;
				P = 1;

				Type = SkillType.ФизическаяАтака;
				TargetType = SkillTargetType.Противник;

				Uron = ОтУровня( 0.2f, 0.35f, 0.5f );
			}
		}


		public class Skill3 : Skill
		{
			public Skill3 ()
			{
				Name = "Взрывной череп";
				Description = "Кидает череп, который взрывается и наносит цели и рядом случайной 10/25/40 ед. урона. Понижает силу и выносливость целей на 10% на один раунд.";
				Id = 3;
			}

			public override void Action ( List<GHero> targets )
			{
				var target = targets[ 0 ];
				var r0 = Logic_DoDamage( Owner, target, SkillType.ФизическийУрон, this, Uron );
				if ( r0.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Взрывной череп от игрока " + Owner.VisualName + " наносит персонажу " + targets[ 0 ].VisualName + " <span class='red'>" + r0.ИтоговыйУрон + "</span> урона." );
				}

				Logic_AddBuffDebuff( Owner, target, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = 3,
					Value = -10
				} );
				Logic_AddBuffDebuff( Owner, target, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Выносливость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = 3,
					Value = -10
				} );
				var enemyHeroes = Battle.Heroes.Where( w => w.Side != Owner.Side && w != target ).ToList();
				if ( enemyHeroes.Count > 0 )
				{
					var addTarget = enemyHeroes.ElementAt( Skill.Random.Next( enemyHeroes.Count ) );
					var r = Logic_DoDamage( Owner, addTarget, SkillType.ФизическийУрон, this, Uron );
					if ( r.Успешно )
					{
						Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Взрывной череп от игрока " + Owner.VisualName + " наносит персонажу " + addTarget.VisualName + " <span class='red'>" + r.ИтоговыйУрон + "</span> урона." );
					}
					Logic_AddBuffDebuff( Owner, addTarget, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = 3,
						Value = -10
					} );
					Logic_AddBuffDebuff( Owner, addTarget, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Выносливость, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = 3,
						Value = -10
					} );
				}
			}

			public int Uron;

			public override void ChangeParams ()
			{
				M = 10;
				O = 3;
				Mr = 2;

				Type = SkillType.ФизическийУрон;
				TargetType = SkillTargetType.Противник;

				Uron = ОтУровня( 10, 25, 40 );
			}
		}


		public class Skill4 : Skill
		{
			public Skill4 ()
			{
				Name = "Ярость";
				Description = "Мародёр впадает в ярость, увеличивая свою ловкость на 20/40/60% на 4 следующих этапа.";
				Id = 4;
			}


			public override void Action ( List<GHero> targets )
			{
				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = Lovkost
				} );
			}

			public int Lovkost;

			public override void ChangeParams ()
			{
				M = 10;
				O = 2;
				P = 5;
				D = 4;

				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;

				Lovkost = ОтУровня( 20, 40, 60 );
			}
		}


		public class Skill5 : Skill
		{
			public Skill5 ()
			{
				Name = "Боевой клич";
				Description = "Увеличивает силу всех союзников и мародёра на 10/14/18%.";
				Id = 5;
			}


			public override void Action ( List<GHero> targets )
			{
				foreach ( var hero in Battle.Heroes.Where( w => w.Side == Owner.Side ) )
				{
					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = Sila
					} );

					Battle.ToLog( Name + " (=>" + hero.VisualName + "): " + hero.VisualName + " получает бонус " + Sila + "% силы!" );
				}
			}

			public int Sila;

			public override void ChangeParams ()
			{
				O = 3;
				P = 5 * 3;
				D = 5;

				Type = SkillType.ФизическийУрон;
				TargetType = SkillTargetType.БезЦели;

				M = ОтУровня( 10, 18, 26 );
				Sila = ОтУровня( 10, 14, 18 );
			}
		}


		public class Skill6 : Skill
		{
			public Skill6 ()
			{
				Name = "Серия атак";
				Description = "Начинает серию атак, наносит 15/20/25% урона от силы. Следующие 6 этапов, каждая физическая атака увеличивает ловкость на 4% и силу на 2%.";
				Id = 6;
			}

			public class Skill6Passive : GPassiveAbility
			{
				public Skill6Passive ()
				{
					Id = 0;
					Name = "Продолжающаяся серия атак";
					Description = "Увеличивает ловкость и силу с каждой физической атакой.";
					EventSubscribeList.Add( "ДоНанесенияУронаПерсонажу" );
					Custom = true;
				}

				public override void Apply ( string eventType, object eventParams, GBattle battle = null )
				{
					if ( eventType == "ПослеНанесенияУронаПерсонажу" )
					{
						var data = ( ПослеНанесенияУронаПерсонажу )eventParams;
						if ( data.Инициатор == Owner && data.ТипУрона == Skill.SkillType.ФизическаяАтака )
						{
							battle.ToLog( Name + ": " + Owner.VisualName + " увеличивает свою ловкость на 4% и силу на 2%" );
						}
					}
				}
			}

			public override void Action ( List<GHero> targets )
			{
				Owner.PassiveAbilities.Add( new Skill6Passive() );

				Battle.ToLog( Name + " (=>" + Owner.VisualName + "): " + "Начинается серия атак!" );
			}

			public int Uron;

			public override void ChangeParams ()
			{
				O = 1;
				P = 8;
				D = 6;

				Type = SkillType.ФизическаяАтака;
				TargetType = SkillTargetType.Противник;

				M = ОтУровня( 15, 11, 7 );
				Uron = ОтУровня( 15, 20, 25 );
			}
		}


		public class Skill7 : Skill
		{
			public Skill7 ()
			{
				Name = "Подлый удар";
				Description = "Наносит урон в размере 10/25/40% ловкости.";
				Id = 7;
			}


			public override void Action ( List<GHero> targets )
			{
				var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическаяАтака, this, Owner.Ловкость.TotalValue * Lovkost );
				if ( r.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Подлый удар от игрока " + Owner.VisualName + ", " + targets[ 0 ].VisualName + " получает урон в размере <span class='red'>" + r.ИтоговыйУрон + "</span>." );
				}
			}

			public float Lovkost;
			public override void ChangeParams ()
			{
				M = 10;
				O = 2;
				P = 5 * 3;

				Type = SkillType.ФизическаяАтака;
				TargetType = SkillTargetType.Противник;

				Lovkost = ОтУровня( 0.10f, 0.25f, 0.40f );
			}
		}


		public class Skill8 : Skill
		{
			public Skill8 ()
			{
				Name = "Чудовищный крик";
				Description = "Понижает ловкость и силу всех противников на 20/35/50% на следующий один раунд.";
				Id = 8;
			}


			public override void Action ( List<GHero> targets )
			{
				foreach ( var hero in Battle.Heroes.Where( w => w.Side != Owner.Side ) )
				{
					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = -Ponizhenie
					} );

					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = -Ponizhenie
					} );
				}
			}

			public int Ponizhenie;

			public override void ChangeParams ()
			{
				O = 3;
				P = 7 * 3;
				D = 3;

				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;

				M = ОтУровня( 20, 30, 40 );
				Ponizhenie = ОтУровня( 20, 35, 50 );

			}
		}


		public class Skill9 : Skill
		{
			public Skill9 ()
			{
				Name = "Прилив силы";
				Description = "3/6/9% ловкости (текущей) переводится в силу до конца боя. ";
				Id = 9;
			}


			public override void Action ( List<GHero> targets )
			{
				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = Lovkost
				} );

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = -Lovkost
				} );
			}

			public int Lovkost;

			public override void ChangeParams ()
			{
				M = 3;
				O = 2;
				D = -1;

				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;

				Lovkost = ОтУровня( 3, 6, 9 );
			}
		}


		public class Skill10 : Skill
		{
			public Skill10 ()
			{
				Name = "Молниеносные атаки";
				Description = "Наносит урон в размере суммы всех параметров равномерно между всеми противникам.";
				Id = 10;
			}


			public override void Action ( List<GHero> targets )
			{
				var summa = Owner.Сила.TotalValue + Owner.Ловкость.TotalValue + Owner.Интеллект.TotalValue + Owner.Выносливость.TotalValue + Owner.Удача.TotalValue;
				var dmg = summa / Battle.Heroes.Count( c => c.Side != Owner.Side );
				foreach (var hero in Battle.Heroes.Where(w=>w.Side != Owner.Side))
				{
					var r = Logic_DoDamage( Owner, hero, SkillType.ФизическаяАтака, this, dmg );
					if ( r.Успешно )
					{
						Battle.ToLog( Name + "(=> " + hero.VisualName + "): Молниеносные атаки наносят адский удар на " + r.ИтоговыйУрон + " единиц." );
					}
					else
					{
						Battle.ToLog( Name + "(=> " + hero.VisualName + "): Молниеносные атаки не наносят урон, ужаснувшись силе врага." );
					}
				}
			}

			public override void ChangeParams ()
			{
				O = 3;
				P = 8 * 3;

				Type = SkillType.ФизическийУрон;
				TargetType = SkillTargetType.БезЦели;

				M = ОтУровня( 35, 45, 55 );
				Mr = ОтУровня( 6, 5, 4 );
			}
		}
	}
}