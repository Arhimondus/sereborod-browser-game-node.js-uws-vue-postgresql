﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEREBOROD
{
	public class Gladiator : HeroType
	{
		public GHero Owner;
		public Gladiator ( GHero owner )
		{
			Owner = owner;
			Skills = new List<Skill>( new Skill[] { new Skill1(), new Skill2(), new Skill3(), new Skill4(), new Skill5(), new Skill6(), new Skill7(), new Skill8(), new Skill9(), new Skill10() } );
			foreach ( var skill in Skills )
			{
				skill.Owner = owner;
				skill.ForClass = "Gladiator";
				skill.Level = 0;
			}
		}

		public static Skill GetSkill ( int id )
		{
			switch ( id )
			{
				case 1:
					return new Skill1();
				case 2:
					return new Skill2();
				case 3:
					return new Skill3();
				case 4:
					return new Skill4();
				case 5:
					return new Skill5();
				case 6:
					return new Skill6();
				case 7:
					return new Skill7();
				case 8:
					return new Skill8();
				case 9:
					return new Skill9();
				case 10:
					return new Skill10();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public class Skill1 : Skill
		{
			public Skill1 ()
			{
				Name = "Стандартная атака";
				Description = "Наносит от 4 до 6-15 урона (основано на Силе), увеличивает ловкость на 1/2/3% до конца боя.";
				Id = 1;
			}

			int Lovkost;
			public override void Action ( List<GHero> targets )
			{
				var uron = ОснованоНаПараметре( Owner.Сила, new[]
				{
					new GParamValue <byte>( 0, 6 ),
					new GParamValue <byte>( 30, 9 ),
					new GParamValue <byte>( 70, 12 ),
					new GParamValue <byte>( 120, 15 )
				} );

				var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическаяАтака, this, Random.Next( 4, uron + 1 ) );

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = -1,
					Value = Lovkost
				} );

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Абсолютное,
					Time = -1,
					Value = 1
				} );

				if ( r.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + Owner.VisualName + " наносит <span class='red'>" + r.ИтоговыйУрон + "</span> урона персонажу " + targets[ 0 ].Name + " и увеличивает свою ловкость на " + Lovkost + "%." );
				}
			}

			public override void ChangeParams ()
			{
				M = 4;
				O = 1;
				P = 0;
				R = 1;
				Lovkost = ОтУровня( 1, 2, 3 );
				TargetType = SkillTargetType.Противник;
			}
		}


		public class Skill2 : Skill
		{
			public Skill2 ()
			{
				Name = "Комбо удар";
				Description = "Наносит 5/12/19% от силы 2-5 раз (основано на Ловкости).";
				Id = 2;
			}

			public override void Action ( List<GHero> targets )
			{
				var raz = ОснованоНаПараметре( Owner.Ловкость, new[]
				{
					new GParamValue <byte>( 0, 2 ),
					new GParamValue <byte>( 50, 3 ),
					new GParamValue <byte>( 100, 4 ),
					new GParamValue <byte>( 150, 5 )
				} );

				for ( byte i = 0; i < raz; i++ )
				{
					var uron = Owner.Сила.TotalValue * Uron;
					var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическаяАтака,this, uron );
					if ( r.Успешно )
					{
						Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + targets[ 0 ].VisualName + " получает урон в размере <span class='red'>" + r.ИтоговыйУрон + "</span> от комбо удара!." );
					}
				}
			}

			float Uron;
			public override void ChangeParams ()
			{
				M = ОтУровня( 10, 15, 20 );
				O = 2;
				P = 3;
				R = 1;
				Uron = ОтУровня( 0.04f, 0.08f, 0.12f );
				Type = SkillType.ФизическаяАтака;
				TargetType = SkillTargetType.Противник;
			}
		}


		public class Skill3 : Skill
		{
			public Skill3 ()
			{
				Name = "Защитная стойка";
				Description = "Увеличивает защиту от физического урона на 22/33/44%.";
				Id = 3;
			}

			public override void Action ( List<GHero> targets )
			{
				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.ФизическаяЗащита, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Абсолютное,					
					Time = 6,
					Value = Pdef
				} );

				Battle.ToLog( Name + " (=>" + Owner.VisualName + "): " + Owner.VisualName + " увеличивает себе защиту от физического урона на <span class='orange'>" + Pdef + "%</span>." ); ;
			}

			int Pdef;
			public override void ChangeParams ()
			{
				M = ОтУровня( 10, 14, 18 );
				Pdef = ОтУровня( 22, 33, 44 );
				P = 12;
				R = 2;
				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;
			}
		}


		public class Skill4 : Skill
		{
			public Skill4 ()
			{
				Name = "Топот богов";
				Description = "Наносит от 10 до 18/32/50% от Силы в виде магического урона каждому противнику (процент на каждого юнита может быть разным). ";
				Id = 4;
			}


			public override void Action ( List<GHero> targets )
			{
				var enemyHeroes = Battle.AliveHeroes.Where( w => w.Side != Owner.Side );
				foreach ( var hero in enemyHeroes )
				{
					var r = Logic_DoDamage( Owner, hero, SkillType.МагическийУрон, this, Random.Next( 10, Uron + 1 ) * Owner.Сила.TotalValue / 100 );
					if ( r.Успешно )
					{
						Battle.ToLog( Name + " (=>" + hero.VisualName + "): " + hero.VisualName + " получает урон в размере <span class='red'>" + r.ИтоговыйУрон + "</span> ужаснувшись гнева богов." );
					}
				}
			}

			public int Uron;

			public override void ChangeParams ()
			{
				M = ОтУровня( 15, 15, 20 );
				Uron = ОтУровня( 18, 32, 50 );
				O = 3;
				P = 5 * 3;
				R = 2;
				Type = SkillType.МагическийУрон;
				TargetType = SkillTargetType.БезЦели;
			}
		}


		public class Skill5 : Skill
		{
			public Skill5 ()
			{
				Name = "Мощнейший удар";
				Description = "Наносит выбранной цели 100% урона от Силы, в случае если здоровье цели станет менее 4/7/12%+0-3%(основано на Удаче), то тогда убивает цель. ";
				Id = 5;
			}


			public override void Action ( List<GHero> targets )
			{
				var Uph = Ph + ОснованоНаПараметре( Owner.Удача, new[]
				{
					new GParamValue <float>( 0, 0.00f ),
					new GParamValue <float>( 45, 0.01f ),
					new GParamValue <float>( 90, 0.02f ),
					new GParamValue <float>( 135, 0.03f )
				} );


				bool uProtivnikaBessmertie = false;

				var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическийУрон,this, Owner.Сила.TotalValue );
				if ( r.Успешно )
				{
					if ( targets[ 0 ].Health < targets[ 0 ].MaxHealth * Uph )
					{
						if ( !uProtivnikaBessmertie )
						{
							if ( targets[ 0 ].Health > 0 )
								targets[ 0 ].Health = 0;
							Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Неожиданно " + targets[ 0 ].VisualName + " получает мощнейший удар на <span class='red'>" + r.ИтоговыйУрон + "</span> урона в спину, который раскалывает её и не оставляет никаких шансов на дальнейшую жизнь." );
						}
						else
						{
							targets[ 0 ].Health = 1;
							Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Неожиданно " + targets[ 0 ].VisualName + " получает мощнейший удар на <span class='red'>" + r.ИтоговыйУрон + "</span> урона в спину, который раскалывает её и не оставляет никаких шансов на дальнешую жизнь. Однако через мгновение " + targets[ 0 ] + " поднимается вновь. Он еще жив!" );
						}
					}
					else
					{
						Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + targets[ 0 ].VisualName + " получает мощнейший удар на <span class='red'>" + r.ИтоговыйУрон + "</span> урона." );
					}
				}

			}

			float Ph;
			public override void ChangeParams ()
			{
				M = 20;
				O = 3;
				P = 5 * 3;
				Mr = 3;
				R = 1;
				Ph = ОтУровня( 0.04f, 0.07f, 0.12f );
				Type = SkillType.ФизическийУрон;
				TargetType = SkillTargetType.Противник;
			}
		}


		public class Skill6 : Skill
		{
			public Skill6 ()
			{
				Name = "Клич победы";
				Description = "Увеличивает выносливость на 20% и силу на 10% всей команды на 2/3/4 раунда.";
				Id = 6;
			}

			public override void Action ( List<GHero> targets )
			{
				foreach ( var hero in Battle.AliveHeroes.Where( w => w.Side == Owner.Side ) )
				{
					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Выносливость, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = 20
					} );

					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = 10
					} );
				}

				Battle.ToLog( Name + ": " + Owner.VisualName + " увеличивает выносливость на 20% и силу на 10% на " + D + " раундов всей команде." );
			}

			public override void ChangeParams ()
			{
				M = ОтУровня( 20, 17, 14 );
				D = ОтУровня( 2 * 3, 3 * 3, 4 * 3 );
				O = 2;
				P = 5 * 3;
				R = 2;
				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;
			}
		}


		public class Skill7 : Skill
		{
			public Skill7 ()
			{
				Name = "Заряд энергии";
				Description = "Увеличивает силу персонажа на 5% на 3/4/5 раунда. Может стакаться.";
				Id = 7;
			}

			public override void Action ( List<GHero> targets )
			{
				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = 5
				} );

				Battle.ToLog( Name + " (=>" + Owner.VisualName + "): " + Owner.VisualName + " заряжается энергией, увеличивая тем самым свою силу на 5% на некоторое время." );
			}

			public override void ChangeParams ()
			{
				M = ОтУровня( 5, 5, 4 );
				D = ОтУровня( 3 * 3, 4 * 3, 5 * 3 );
				O = 1;
				R = 2;
				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.БезЦели;
			}
		}


		public class Skill8 : Skill
		{
			public Skill8 ()
			{
				Name = "Манёвр";
				Description = "Неожиданный манёвр позволяющий нанести 20/30/40% от силы и  40/55/70% шанс продолжить атаку серией ударов в количестве 1-3(основано на Ловкости) текущим уровнем стандартной атаки.";
				Id = 8;
			}


			public override void Action ( List<GHero> targets )
			{
				var udarov = ОснованоНаПараметре( Owner.Ловкость, new[]
				{
					new GParamValue <byte>( 0, 1 ),
					new GParamValue <byte>( 50, 2 ),
					new GParamValue <byte>( 100, 3 )
				} );

				var r = Logic_DoDamage( Owner, targets[ 0 ], SkillType.ФизическийУрон,this, Sila * Owner.Сила.TotalValue / 100 );
				if ( r.Успешно )
				{
					Battle.ToLog( Name + " (=>" + targets[ 0 ].VisualName + "): " + "Неожиданный манёвр от персонажа " + Owner.VisualName + " позволил нанести " + targets[ 0 ].Name + " <span class='red'>" + r.ИтоговыйУрон + "</span> урона." );
				}

				if ( Random.Next( 100 ) <= Chance )
				{
					for ( int i = 0; i < udarov; i++ )
					{
						Owner.ActiveAbilities[ 0 ].Action( new List<GHero>( new[] { targets[ 0 ] } ) );
					}
				}
			}

			int Sila;
			int Chance;
			public override void ChangeParams ()
			{
				M = ОтУровня( 6, 5, 4 );
				Sila = ОтУровня( 20, 30, 40 );
				Chance = ОтУровня( 40, 55, 70 );
				O = 2;
				P = 1;
				Mr = 2;
				R = 1;
				Type = SkillType.ФизическийУрон;
				TargetType = SkillTargetType.Противник;
			}
		}


		public class Skill9 : Skill
		{
			public Skill9 ()
			{
				Name = "Защитник";
				Description = "Защищает выбранного союзника, перенаправляя все направленные на него способности в этом раунде (те которые уже после данной способности будут совершать действия) на гладатора, но гладиатор получает на 25/15/10% урона больше.";
				Id = 9;
			}

			public override void ContinueAction ( object rawData )
			{
				var data = ( Skill9Continue )rawData;
				var slotsToThatTarget = new List<GClientSkillParam>();
				foreach ( var hero in Battle.AliveHeroes )
				{
					var slot = hero.Slots[ Battle.Step ];
					if ( slot != null && slot.Targets != null && slot.Targets.Any( a => a == data.Target.Name ) )
					{
						slot.Targets.Remove( data.Target.Name );
						slot.Targets.Add( Owner.Name );
					}
				}

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Баффы, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					Time = 3
				} );

				Battle.ToLog( Name + "(=>" + data.Target + "): Гладиатор " + Owner.VisualName + " защищает персонажа " + data.Target + " от всех направленных атак!" );
			}

			public class Skill9Continue
			{
				public GHero Target;
				public float Uron;
			}

			public override void Action ( List<GHero> targets )
			{
				//Logic_AddBuffDebuff( Owner, targets[ 0 ], Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Баффы, new GValue
				//{
				//    Type = GValue.ValueType.АктивнаяСпособность,
				//    Time = 3,
				//    ContinueData = new Skill9Continue { Target = targets[ 0 ], Uron = Uron }
				//} );

				//Logic_AddBuffDebuff( Owner, targets[ 0 ], Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.ПовышениеЗащиты, new GValue
				//{
				//    Type = GValue.ValueType.АктивнаяСпособность,
				//    NumberType = GValue.ValueNumberType.Процентное,
				//    Time = 3,
				//    Value = ( int )-Uron * 100,
				//    ContinueData = Uron
				//} );

				foreach ( var sd in Battle.BattleSkills )
				{
					if ( sd.Targets.Any( a => a == targets[ 0 ] ) )
					{
						sd.Targets.Remove( sd.Targets.First( f => f == targets[ 0 ] ) );
						sd.Targets.Add( Owner );
					}
				}

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.ПовышениеЗащиты, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Абсолютное,
					Time = GetTime(Battle.Step),
					Value = ( int )-Uron * 100,
					ContinueData = Uron
				} );
			}

			public int GetTime ( short step )
			{
				if ( step == 2 )
					return 1;
				else if ( step == 1 )
					return 2;
				else
					return 3;
			}

			float Uron;
			public override void ChangeParams ()
			{
				M = ОтУровня( 13, 12, 10 );
				Uron = ОтУровня( 0.25f, 0.15f, 0.10f );
				R = 3;
				Type = SkillType.ФизическаяСпособность;
				TargetType = SkillTargetType.СоюзникБезТекущегоГероя;
			}
		}


		public class Skill10 : Skill
		{
			public Skill10 ()
			{
				Name = "Ульта:В Атаку!";
				Description = "Гладиатор получает в 2-1.25(основано на Удаче) раза больше урона, но усиляет союзников на 15/25/45% силы и ловкости (их параметров), сам же гладиатор получает бонус только 15% к силе.";
				Id = 10;
			}


			public override void Action ( List<GHero> targets )
			{
				int udamage = ОснованоНаПараметре( Owner.Удача, new[]
				{
					new GParamValue <int>( 0, -200 ),
					new GParamValue <int>( 35, -175 ),
					new GParamValue <int>( 70, -150 ),
					new GParamValue <int>( 105, -125 )
				} );

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.ПовышениеЗащиты, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = udamage
				} );

				foreach ( var hero in Battle.AliveHeroes.Where(w=>w.Side == Owner.Side && w != Owner ))
				{
					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = Bonus
					} );

					Logic_AddBuffDebuff( Owner, hero, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
					{
						Type = GValue.ValueType.АктивнаяСпособность,
						NumberType = GValue.ValueNumberType.Процентное,
						Time = D,
						Value = Bonus
					} );

					Battle.ToLog( Name + "(=> " + hero.VisualName +"): Бонус " + Bonus + "% к силе и ловкости." );
				}

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Сила, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = 15
				} );

				Logic_AddBuffDebuff( Owner, Owner, Skill.SkillType.ФизическаяСпособность, this, GParam.ParamType.Ловкость, new GValue
				{
					Type = GValue.ValueType.АктивнаяСпособность,
					NumberType = GValue.ValueNumberType.Процентное,
					Time = D,
					Value = 15
				} );

				Battle.ToLog( Name + "(=> " + Owner.VisualName + "): Бонус 15% к силе и ловкости, но гладиатор будет получать на " + udamage + "% больше урона." );
			}

			public int Bonus;

			public override void ChangeParams ()
			{
				M = 10;
				R = 3;
				D = 3 * 3;
				Bonus = ОтУровня( 15, 25, 45 );
				Mr = 5;
				P = 10 * 3;
			}
		}
	}
}