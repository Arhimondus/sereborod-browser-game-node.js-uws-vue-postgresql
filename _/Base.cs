﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;


namespace SEREBOROD
{
	public class Base
	{
	}


	public class GHeroView
	{
		public int Сила;
		public int Ловкость;
		public int Интеллект;
		public int Удача;
		public int Выносливость;

		public int ФизическаяЗащита;
		public int МагическаяЗащита;

		public string Персонаж;
		public string Класс;

		public int СвободныеПараметры;
	}


	public class GParam
	{
		public enum ParamType
		{
			Сила,
			Ловкость,
			Интеллект,
			Выносливость,
			Удача,
			ФизическаяЗащита,
			МагическаяЗащита,
			ПовышениеУрона,
			ПовышениеЗащиты,
			Баффы
		}


		public ParamType Type;
		public List<GValue> Value = new List<GValue>();

		public int TotalValue
		{
			get
			{
				var absoluteValues = Value.Where( w => w.NumberType == GValue.ValueNumberType.Абсолютное ).Select( s => s.Value );
				var av = 0;
				if ( absoluteValues.Count() > 0 )
					av = absoluteValues.Aggregate( ( one, two ) => one + two );

				var percentageValues = Value.Where( w => w.NumberType == GValue.ValueNumberType.Процентное ).Select( s => s.Value );
				var pv = 0;
				if ( percentageValues.Count() > 0 )
					pv = percentageValues.Aggregate( ( one, two ) => one + two );


				av = av + av * pv / 100;

				return av;
			}
		}


		public GParam ()
		{
		}

		public GParam ( ParamType type )
		{
			Type = type;
		}

		public GParam ( ParamType type, GValue initial )
		{
			Type = type;
			Value.Add( initial );
		}


		public int HardValue
		{
			get
			{
				var vs = Value.FirstOrDefault( f => f.Type == GValue.ValueType.Параметр );
				return vs != null ? vs.Value : 0;
			}

			set
			{
				var vs = Value.FirstOrDefault( f => f.Type == GValue.ValueType.Параметр );
				vs.Value = value;
			}
		}

		public int ItemsValue
		{
			get
			{
				var vs = Value.FirstOrDefault( f => f.Type == GValue.ValueType.Предмет );
				return vs != null ? vs.Value : 0;
			}
		}

		public int AbilityValue
		{
			get
			{
				var vs = Value.FirstOrDefault( f => f.Type == GValue.ValueType.ПассивнаяСпособность );
				return vs != null ? vs.Value : 0;
			}
		}

		public int PermanentValue
		{
			get { return HardValue + ItemsValue + AbilityValue; }
		}

		public int FloatValue
		{
			get
			{
				var vs = Value.FirstOrDefault( f => f.Type == GValue.ValueType.АктивнаяСпособность );
				return vs != null ? vs.Value : 0;
				//return TotalValue - HardValue - ItemsValue;
			}
		}
	}


	public class GValue
	{
		public enum ValueType
		{
			Параметр,
			Предмет,
			АктивнаяСпособность,
			ПассивнаяСпособность
		}


		public enum ValueNumberType
		{
			Абсолютное,
			Процентное
		}


		public int Value;
		public ValueType Type;
		public ValueNumberType NumberType;

		[NonSerialized]
		public GHero From;
		[NonSerialized]
		public GHero To;
		[NonSerialized]
		public Skill Skill;
		[NonSerialized]
		public object ContinueData;
		public bool ContinueAction;

		public int SkillVarId;

		public int Time = -1;
	}


	public class HeroType
	{
		public List<Skill> Skills = new List<Skill>();
	}
}