﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEREBOROD
{
	public abstract partial class Skill
	{
		public static Result_RestoreHealth Logic_RestoreHealth ( GHero from, GHero to, SkillType type, double rawValue )
		{
			return Logic_RestoreHealth( from, to, type, ( int )Math.Ceiling( rawValue ) );
		}

		public static Result_RestoreHealth Logic_RestoreHealth ( GHero from, GHero to, SkillType type, int rawValue )
		{
			if ( rawValue <= 0 ) return new Result_RestoreHealth { ИтоговоеЗначение = 0 };
			to.Health += rawValue;
			if ( to.Health > to.MaxHealth )
			{
				to.Health = to.MaxHealth;
				rawValue -= ( to.Health - to.MaxHealth );
			}
			return new Result_RestoreHealth { ИтоговоеЗначение = rawValue };
		}

		public class Result_RestoreHealth
		{
			public int ИтоговоеЗначение;
		}

		public class Result_RestoreEnergy
		{
			public int ИтоговоеЗначение;
		}

		public static Result_RestoreEnergy Logic_RestoreEnergy ( GHero from, GHero to, SkillType type, int rawValue )
		{
			if(rawValue <= 0) return new Result_RestoreEnergy { ИтоговоеЗначение = 0 };
			to.Energy += rawValue;
			if ( to.Energy > to.MaxEnergy )
			{
				to.Energy = to.MaxEnergy;
				rawValue -= ( to.Energy - to.MaxEnergy );
			}
			return new Result_RestoreEnergy { ИтоговоеЗначение = rawValue };
		}

		public static Result_RestoreEnergy Logic_RemoveEnergy ( GHero from, GHero to, SkillType type, int rawValue )
		{
			if ( rawValue <= 0 ) return new Result_RestoreEnergy { ИтоговоеЗначение = 0 };
			to.Energy -= rawValue;
			if ( to.Energy < 0 )
				to.Energy = 0;
			return new Result_RestoreEnergy { ИтоговоеЗначение = rawValue };
		}

		public class Result_DoDamage
		{
			public int ИтоговыйУрон;
			public bool Успешно;
			public string ДополнительноеСообщение;
		}

		public static Result_DoDamage Logic_DoDamage ( GHero from, GHero to, SkillType type, Skill skill, double rawValue )
		{
			return Logic_DoDamage( from, to, type, skill, ( int )Math.Ceiling( rawValue ) );
		}

		public static Result_DoDamage Logic_DoDamage ( GHero from, GHero to, SkillType type, Skill skill, float rawValue )
		{
			return Logic_DoDamage( from, to, type, skill, ( int )Math.Ceiling( rawValue ) );
		}

		public static Result_DoDamage Logic_DoDamage ( GHero from, GHero to, SkillType type, Skill skill, int rawValue )
		{
			//Проверка пассивок
			//todo...

			IntegerRef value = new IntegerRef( rawValue );
			IntegerRef dmg = new IntegerRef( 0 );
			switch ( skill.Type )
			{
				case SkillType.ФизическаяАтака:
				case SkillType.ФизическийУрон:
					DoEventActions( "ДоНанесенияУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = value, Способность = skill, ТипУрона = type }, from.Battle );
					DoEventActions( "ДоНанесенияФизическогоУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = value, Способность = skill, ТипУрона = type }, from.Battle );

					dmg = new IntegerRef( value.Value * ( 100 - to.ФизическаяЗащита.TotalValue ) / 100 );
					to.Health -= dmg.Value;
					DoEventActions( "ПослеНанесенияУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = dmg, ТипУрона = type }, from.Battle );
					DoEventActions( "ПослеНанесенияФизическогоУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = dmg, ТипУрона = type }, from.Battle );
					break;
				case SkillType.МагическаяАтака:
				case SkillType.МагическийУрон:
					DoEventActions( "ДоНанесенияУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = value, Способность = skill, ТипУрона = type }, from.Battle );
					DoEventActions( "ДоНанесенияМагическогоУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = dmg, Способность = skill, ТипУрона = type }, from.Battle );

					dmg = new IntegerRef( value.Value * ( 100 - to.МагическаяЗащита.TotalValue ) / 100 );
					to.Health -= dmg.Value;
					DoEventActions( "ПослеНанесенияУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = value, ТипУрона = type }, from.Battle );
					DoEventActions( "ПослеНанесенияМагическогоУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = dmg, ТипУрона = type }, from.Battle );
					break;
				case SkillType.ЧистыйУрон:
					DoEventActions( "ПослеНанесенияУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = value, Способность = skill, ТипУрона = type }, from.Battle );
					DoEventActions( "ДоНанесенияЧистогоУронаПерсонажу", new ДоНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, ПредполагаемыйУрон = dmg, Способность = skill, ТипУрона = type }, from.Battle );

					dmg = new IntegerRef( value.Value );
					to.Health -= dmg.Value;
					DoEventActions( "ПослеНанесенияУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = value, ТипУрона = type }, from.Battle );
					DoEventActions( "ПослеНанесенияЧистогоУронаПерсонажу", new ПослеНанесенияУронаПерсонажу { Жертва = to, Инициатор = from, Урон = dmg, ТипУрона = type }, from.Battle );
					break;
				default:
					break;
			}

			return new Result_DoDamage { ИтоговыйУрон = dmg.Value, Успешно = true };
		}

		public static void Login_Kill ( GHero from, GHero to )
		{
			DoEventActions( "ДоСмертиПерсонажа", new ДоСмертиПерсонажа { Жертва = to, ПредполагаемыйУбийца = from, УронКоторогоДостаточноДляУбийства = new IntegerRef( -1 ) }, from.Battle );
			to.Health = 0;
			DoEventActions( "ПослеСмертиПерсонажа", new ДоСмертиПерсонажа { Жертва = to, ПредполагаемыйУбийца = from, УронКоторогоДостаточноДляУбийства = new IntegerRef( -1 ) }, from.Battle );
		}

		public static void Logic_AddBuffDebuff ( GHero from, GHero to, SkillType skill_type, Skill skill, GParam.ParamType param_type, GValue value )
		{
			value.Type = GValue.ValueType.АктивнаяСпособность;
			value.From = from;
			value.To = to;
			value.Skill = skill;
			to.Params.FirstOrDefault( f => f.Type == param_type ).Value.Add( value );
		}
	}
}