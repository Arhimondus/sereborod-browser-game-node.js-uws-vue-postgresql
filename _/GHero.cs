﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

using System;
using System.Linq;
using MongoDB.Bson;

namespace SEREBOROD
{
	public partial class GHero : RefreshableObject
	{
		public GHero ( string id )
		{
			CollectionName = "Hero";
			var cn = Database.GetCollection( CollectionName );
			var @object = cn.FindOneByIdAs<MHero>( ObjectId.Parse( id ) );
			if ( @object == null )
				throw new ArgumentException();
			DatabaseObject = @object;
			Constructor();
		}

		private MHero Hero { get { return ( MHero )DatabaseObject; } }

		public System.String _Идентификатор
		{
			get
			{
				return Hero.Идентификатор;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Идентификатор = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.String _Имя
		{
			get
			{
				return Hero.Имя;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Имя = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Collections.Generic.List<System.String> _Предметы
		{
			get
			{
				return Hero.Предметы;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Предметы = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Сила
		{
			get
			{
				return Hero.Сила;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Сила = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Ловкость
		{
			get
			{
				return Hero.Ловкость;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Ловкость = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Интеллект
		{
			get
			{
				return Hero.Интеллект;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Интеллект = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Удача
		{
			get
			{
				return Hero.Удача;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Удача = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Выносливость
		{
			get
			{
				return Hero.Выносливость;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Выносливость = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public SEREBOROD.MHero.ТипГероя _Тип
		{
			get
			{
				return Hero.Тип;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Тип = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Byte _СвободныеПараметры
		{
			get
			{
				return Hero.СвободныеПараметры;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.СвободныеПараметры = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Byte _СвободныеСпособности
		{
			get
			{
				return Hero.СвободныеСпособности;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.СвободныеСпособности = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Byte _ИгрНаПассивныеСпособности
		{
			get
			{
				return Hero.ИгрНаПассивныеСпособности;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.ИгрНаПассивныеСпособности = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Byte _Уровень
		{
			get
			{
				return Hero.Уровень;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Уровень = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Int16 _Опыт
		{
			get
			{
				return Hero.Опыт;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.Опыт = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Collections.Generic.List<SEREBOROD.MPassiveAbility> _ПассивныеСпособности
		{
			get
			{
				return Hero.ПассивныеСпособности;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.ПассивныеСпособности = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}

		public System.Collections.Generic.List<SEREBOROD.MActiveAbility> _АктивныеСпособности
		{
			get
			{
				return Hero.АктивныеСпособности;
			}
			set
			{
				bool quickContextOpen = !IsContextOpen;
				if ( quickContextOpen ) OpenContext();
				Hero.АктивныеСпособности = value;
				if ( quickContextOpen ) SaveAndCloseContext();
			}
		}
	}
}