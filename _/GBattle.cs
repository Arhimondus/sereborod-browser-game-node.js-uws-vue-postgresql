﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SEREBOROD
{
	[Serializable]
	public class GBattle
	{
		public static List<GBattle> AllBattles = new List<GBattle>();

		[NonSerialized]
		public List<GHero> Heroes = new List<GHero>();

		[JsonIgnore]
		public List<GHero> AliveHeroes
		{
			get
			{
				return Heroes.Where( w => w.Alive ).ToList();
			}
		}

		[JsonIgnore]
		public List<GHero> NotAliveHeroes
		{
			get
			{
				return Heroes.Where( w => !w.Alive ).ToList();
			}
		}

		public int Id = NewId++;

		public static int NewId = 1;

		public int CurrentRound = 0;
		public int HeroesCountForStart = 1; //для каждой стороны, если 1, то игра 1 на 1, если 2, то игра 2 на 2

		public int MinHeroLevel = 1;
		public int MaxHeroLevel = 20;

		public bool IsEnd = false;

		public List<string> LogMessages = new List<string>();
		public void ToLog ( string message )
		{
			LogMessages.Add( message );
		}

		private void DecreaseTime ()
		{
			foreach ( var hero in AliveHeroes )
			{
				foreach ( var param in hero.Params )
				{
					var values = param.Value.Where( w => w.Type == GValue.ValueType.АктивнаяСпособность && w.Time >= 1 ).ToList();
					for ( var i = 0; i < values.Count; i++ )
						values[ i ].Time--;
					values = param.Value.Where( w => w.Type == GValue.ValueType.АктивнаяСпособность && w.Time == 0 ).ToList();
					foreach ( var value in values )
						param.Value.Remove( value );
				}

				foreach ( var skill in hero.ActiveAbilities.Where( w => w.Ptimer > 0 ) )
				{
					skill.Ptimer--;
				}

				foreach ( var pa in hero.PassiveAbilities.Where( w => w.Custom && w.D > 0 ) )
				{
					pa.D--;
				}
				hero.PassiveAbilities.RemoveAll( r => r.Custom && r.D == 0 );
			}
		}


		public void CheckDeath ()
		{
			var heroForDelete = Heroes.Where( w => w.Health <= 0 ).ToList();
			foreach ( var hero in heroForDelete )
			{
				hero.Slots = null;
				hero.Alive = false;
				//Heroes.Remove( hero );
				//NotAliveHeroes.Add( hero );
				//hero.Battle = null;
				ToLog( "Персонаж <span class='hero'>" + hero.Name + "</span> скончался." );
			}
		}

		public short Step;

		[NonSerialized]
		public IEnumerable<SkillData> BattleSkills;
		public void DoAllActions ()
		{
			for ( short step = 0; step < 3; step++ )
			{
				Step = step;
				DoContinueActionSkills();
				DecreaseTime();
				BattleSkills = GetAllSkills( step );
				foreach ( var skill in BattleSkills.OrderByDescending( o => o.Skill.R ) )
				{
					skill.Skill.Action( skill.Targets );
					skill.Skill.Ptimer = skill.Skill.P;
				}
				CheckDeath();
				if ( CheckEnd() )
					return;
			}

			foreach ( var hero in AliveHeroes )
			{
				hero.Energy += 3;
				if ( hero.Energy > hero.MaxEnergy )
					hero.Energy = hero.MaxEnergy;
			}

			NextRound();
		}

		public bool CheckEnd ()
		{
			if ( AliveHeroes.Count <= 1 )
			{
				IsEnd = true;
				if ( AliveHeroes.Count == 1 )
				{
					var winSide = Heroes.Where( w => w.Side == AliveHeroes[ 0 ].Side ).ToList();
					var loseSide = Heroes.Where( w => w.Side != AliveHeroes[ 0 ].Side ).ToList();
					foreach ( var hero in winSide )
					{
						MHero.AddExperience( hero, 50 );
						ToLog( "<span class='hero'>" + hero.Name + "</span> +50 опыта" );
					}
					foreach ( var hero in loseSide )
					{
						MHero.AddExperience( hero, 20 );
						ToLog( "<span class='hero'>" + hero.Name + "</span> +20 опыта" );
					}
				}
				else
				{
					foreach ( var hero in Heroes )
					{
						MHero.AddExperience( hero, 33 );
						ToLog( "<span class='hero'>" + hero.Name + "</span> +33 опыта" );
					}
				}
				return true;
			}
			return false;
		}

		private void NextRound ()
		{
			foreach ( var gHero in AliveHeroes )
				gHero.Slots = null;
			CurrentRound++;
		}

		private void DoContinueActionSkills ()
		{
			foreach ( var hero in AliveHeroes )
			{
				if ( hero.Баффы.Value.Any() )
				{
					foreach ( var value in hero.Баффы.Value )
					{
						value.Skill.ContinueAction( value.ContinueData );
					}
				}
			}
		}

		//[Obsolete]
		//private IEnumerable<Skill> GetAllSkills ()
		//{
		//    foreach ( var gHero in Heroes )
		//        foreach ( var slot in gHero.Slots.Where( w => w != null ) )
		//        {
		//            var skill = gHero.ActiveAbilities.FirstOrDefault( f => f.Id == slot.Id );
		//            if ( skill != null )
		//            {
		//                skill.Targets = GHero.GetHeroes( slot.Targets ).ToList();
		//                yield return skill;
		//            }
		//        }
		//}

		delegate void SkillAction ();

		public class SkillData
		{
			public List<GHero> Targets;
			public Skill Skill;
		}

		public bool CheckTargets ( SkillData sd )
		{
			if ( sd.Targets != null )
			{
				foreach ( var hero in sd.Targets )
				{
					if ( hero == null )
						return false;
					if ( !sd.Skill.Owner.Battle.Heroes.Contains( hero ) )
						return false;
					switch ( sd.Skill.TargetType )
					{
						case Skill.SkillTargetType.Противник:
							if ( !sd.Skill.Owner.Battle.AliveHeroes.Where( w => w.Side != sd.Skill.Owner.Side ).Contains( hero ) )
								return false;
							break;
						case Skill.SkillTargetType.Союзник:
							if ( !sd.Skill.Owner.Battle.AliveHeroes.Where( w => w.Side == sd.Skill.Owner.Side ).Contains( hero ) )
								return false;
							break;
						case Skill.SkillTargetType.СоюзникБезТекущегоГероя:
							if ( !sd.Skill.Owner.Battle.AliveHeroes.Where( w => w.Side == sd.Skill.Owner.Side ).Contains( hero ) )
								return false;
							if ( sd.Skill.Owner == hero )
								return false;
							break;
					}
				}
			}

			return true;
		}

		private IEnumerable<SkillData> GetAllSkills ( int step )
		{
			foreach ( var gHero in AliveHeroes )
			{
				if ( gHero.Slots.Count < 3 ) continue;

				var slot = gHero.Slots[ step ];
				if ( slot == null || slot.Id == 0 ) continue;
				var skill = gHero.ActiveAbilities.FirstOrDefault( f => f.Id == slot.Id );

				if ( skill.O == 2 )
				{
					if ( step == 2 )
						continue;
					if ( step == 1 )
						gHero.Slots[ 2 ] = null;
					if ( step == 0 )
						gHero.Slots[ 1 ] = null;
				}

				if ( skill.O == 3 )
				{
					if ( step == 0 )
					{
						gHero.Slots[ 1 ] = null;
						gHero.Slots[ 2 ] = null;
					}
					else
						continue;
				}

				if ( skill.M <= gHero.Energy )
					gHero.Energy -= skill.M;
				else
					continue;

				if ( skill.Ptimer > 0 )
					continue;

				if ( skill != null )
				{
					var sd = new SkillData { Targets = GHero.GetHeroes( slot.Targets ).ToList(), Skill = skill };
					if ( CheckTargets( sd ) )
						yield return sd;
				}
			}
		}
	}


}