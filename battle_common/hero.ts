/// <reference path="battle.ts" />
import { ActiveSkill, PassiveSkill, Slot, Skill } from './battle';
import * as C from './common';

export class Hero {
	id: number;
	name: string;
	class: string;
	level: number;

	slots: Slot[];
	death_type: DeathType = DeathType.normal;
	alive: boolean;
	side: number;

	active_skills: ActiveSkill[];
	passive_skills: PassiveSkill[];

	params: Param[];

	energy: number;
	max_energy: number;

	health: number;
	max_health: number;

	modifiers: Modifier[] = [];

	_chero: C.Hero;

	constructor(_chero: C.Hero) {
		this._chero = _chero;
	};

	pre_init_hero() {
		this.active_skills
			.filter(a => a.naked.initialize)
			.forEach((a) => a.naked.initialize(a, this));
		this.passive_skills
			.filter(a => a.naked.initialize)
			.forEach((p) => p.naked.initialize(p, this));
	};

	get strength() {
		return this.params.find(f => f.type == ParamType.strength);
	};
	get agility() {
		return this.params.find(f => f.type == ParamType.agility);
	};
	get intelligence() {
		return this.params.find(f => f.type == ParamType.intelligence);
	};
	get luck() {
		return this.params.find(f => f.type == ParamType.luck);
	};
	get endurance() {
		return this.params.find(f => f.type == ParamType.endurance);
	};
	get physical_protection() {
		return this.params.find(f => f.type == ParamType.physical_protection);
	};
	get magical_protection() {
		return this.params.find(f => f.type == ParamType.magical_protection);
	};
	get damage_increased() {
		return this.params.find(f => f.type == ParamType.damage_increased);
	};
	get protection_increased() {
		return this.params.find(f => f.type == ParamType.protection_increased);
	};
	// get special_buffs() {
	// 	return this.params.find(f => f.type == ParamType.special_buff);
	// };

	serialize() {
		return {
			id: this.id,
			name: this.name,
			class: this.class,
			level: this.level,

			alive: this.alive,
			side: this.side,

			energy: this.energy,
			max_energy: this.max_energy,

			health: this.health,
			max_health: this.max_health,

			active_skills: this.active_skills.map(m => m.serialize()),
			passive_skills: this.passive_skills.map(m => m.serialize()),

			strength: this.strength.serialize(),
			agility: this.agility.serialize(),
			intelligence: this.intelligence.serialize(),
			luck: this.luck.serialize(),
			stamina: this.endurance.serialize(),
			physical_protection: this.physical_protection.serialize(),
			magical_protection: this.magical_protection.serialize(),
			damage_increased: this.damage_increased.serialize(),
			protection_increased: this.protection_increased.serialize(),
			//special_buffs: this.special_buffs.serialize(),
		};
	};
}

export enum DeathType {
	normal
}

export enum ParamType {
	strength,
	agility,
	intelligence,
	endurance,
	luck,
	physical_protection,
	magical_protection,
	damage_increased,
	protection_increased,
	special_buff
}

export class Param {
	type: ParamType;
	values: Value[] = [];
	owner: Hero;

	constructor(owner: Hero, type: ParamType, initial: Value = null) {
		this.owner = owner;
		this.type = type;

		let that = this;
		this.values.push = function () {
			Array.prototype.forEach.call(arguments, (el) => el = that);
			return Array.prototype.push.apply(this, arguments);
		}

		if (initial != null)
			this.values.push(initial);
	}

	compute(values) {
		let absoluteValues = values.filter(v => v.number_type == ValueNumberType.absolute).map(v => v.value);
		let av = 0;
		if (absoluteValues.length > 0)
			av = absoluteValues.reduce((prev, curr) => prev + curr);

		let percentageValues = values.filter(v => v.number_type == ValueNumberType.percentage).map(v => v.value);
		let pv = 0;
		if (percentageValues.length > 0)
			pv = percentageValues.reduce((prev, curr) => prev + curr);

		av = av + av * pv / 100;

		return av;
	};

	get total_value(): number {
		return this.compute(this.values);
	};

	get base_value(): number {
		return this.compute(this.values.filter(f => f.type === ValueType.item || f.type === ValueType.param));
	};

	serialize() {
		let base = this.base_value;
		let total = this.total_value;
		let relativity;
		if (base == total)
			relativity = 0;
		else if (total > base)
			relativity = 1;
		else if (total < base)
			relativity = -1;

		return {
			value: total,
			relativity,
		};
	};
}

export enum ValueNumberType {
	absolute,
	percentage,
}

export class Value {
	constructor(type: ValueType, value: number, number_type: ValueNumberType = ValueNumberType.absolute, elapsed_time?: number) {
		this.type = type;
		this.number_type = number_type;
		this.value = value;
		this.elapsed_time = elapsed_time;
	}

	param: Param;
	value: number;
	type: ValueType;
	number_type: ValueNumberType;

	//[NonSerialized]
	from: Hero;
	//[NonSerialized]
	to: Hero;
	//[NonSerialized]
	skill: ActiveSkill | PassiveSkill;
	//[NonSerialized]

	continue_data: any;
	continue_action: boolean;

	skill_var_id: number;
	elapsed_time?: number; //??

	remove() {
		if (this.type == ValueType.param) //Значение типа параметр удалить нельзя
			return;
		this.param.values.splice(this.param.values.indexOf(this), 1);
	}
}

export enum ValueType {
	active_ability,
	passive_ability,
	item,
	param,
}

export class TypedValue {
	param_type: ParamType;
	value: Value;
	constructor(param_type: ParamType, value: Value) {
		this.param_type = param_type;
		this.value = value;
	}
}

export class Modifier {
	modifier_id: number;
	skill: Skill;
	values: TypedValue[];
	continue_data: any;
	owner: Hero;

	constructor(modifier_id: number, skill: Skill, owner: Hero, values: TypedValue[] = null, continue_data = null) {
		this.modifier_id = modifier_id;
		this.owner = owner;
		this.skill = skill;
		this.values = values;
		this.continue_data = continue_data;
	};

	remove() {
		this.values.forEach(v => v.value.remove());
		this.owner.modifiers.splice(this.owner.modifiers.indexOf(this), 1);
	};
}

export class ParamModifier extends Modifier {
	constructor(modifier_id: number, skill: Skill, target: Hero, values: TypedValue[]) {
		super(modifier_id, skill, target, values);
		values.forEach(tv => target.params.find(p => p.type == tv.param_type).values.push(tv.value));
	};
}

export class ActionModifier extends Modifier {
	constructor(modifier_id: number, skill: Skill, target: Hero, continue_data: any) {
		super(modifier_id, skill, target, null, continue_data);
	}
}

