/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../hero.ts" />
import { Battle, ActiveNakedSkill, ActiveSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { Hero } from '../hero';

/**
 * Наносит урон в размере %{1} от силы, в случае если у противника остаётся менее 30-60%(основано на Удаче) здоровья, то парализует цель на ${2} О.
 * @name Стремительная атака
 */
export = {
	id: 2,
	class: "maroder",
	params: [[0.20, 0.30, 0.35], [1]],

	mana_cost: [6],			
	action_points: [2],
	cooldown: [1], 
	// step_duration: [0],
	minimal_round: [0],
	
	type: SkillType.physicalAttack,
	targets: [{ unit_number: 1, target_type: SkillTargetType.enemy } as SkillTarget],
	up_level: [2, 6, 12],

	apply($, targets) {
        $.make_damage($.owner, $.single(targets), SkillType.physicalAttack, $.by_level_param() * $.owner.strength.total_value);
        if($.single(targets).health / $.single(targets).max_health < $.base_on_param($.owner.luck.total_value,[[150,0.60],[100, 0.50], [60, 0.45], [30, 0.40], [0, 0.30]]))
            $.paralyze($.owner, $.single(targets), this.type, $.by_level_param(2));
	},
} as ActiveNakedSkill;