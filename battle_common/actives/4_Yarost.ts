/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../hero.ts" />
import { Battle, ActiveNakedSkill, ActiveSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { Hero, TypedValue, ParamType, Value, ValueType, ValueNumberType } from '../hero';

/**
 * Мародёр впадает в ярость, увеличивая свою ловкость на %{1} на ${2} следующих этапа.
 * @name Ярость (Неистовство)
 */
export = {
	id: 4,
	class: "maroder",
	params: [[0.2, 0.4, 0.6], [4]],

	mana_cost: [10],
	action_points: [2],
	cooldown: [5 * 3],

	type: SkillType.physicalSkill,
	targets: [{ unit_number: 1, target_type: SkillTargetType.no_target } as SkillTarget],
	up_level: [2, 5, 10],

	apply($, targets) {
		$.apply_param_modifier(1, $.owner,
			[new TypedValue(ParamType.agility, new Value(ValueType.active_ability, $.by_level_param(1), ValueNumberType.percentage, $.by_level_param(2)))])
	},
} as ActiveNakedSkill;