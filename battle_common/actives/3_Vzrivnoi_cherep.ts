/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../hero.ts" />
import { Battle, ActiveNakedSkill, ActiveSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { Hero, TypedValue, ParamType, Value, ValueType, ValueNumberType } from '../hero';

/**
 * Кидает череп, который взрывается и наносит цели и рядом случайной ${1} ед. урона. Понижает силу и выносливость целей на %{2} на ${3} раунд.
 * @name Взрывной череп
 */
export = {
	id: 2,
	class: "maroder",
	params: [[10, 25, 40], [0.10], [1]],

	mana_cost: [10],
	action_points: [3],
	cooldown: [0],
	// step_duration: [0],
	minimal_round: [2],

	type: SkillType.physicalDamage,
	targets: [{ unit_number: 1, target_type: SkillTargetType.enemy } as SkillTarget],
	up_level: [3, 7, 13],

	apply($, targets) {
		$.make_damage($.owner, $.single(targets), this.type, $.by_level_param(1));
		let second_target = $.battle.heroes_alive.filter(h => h.side != $.owner.side && h != $.single(targets)).sort(s => 0.5 - Math.random())[0];
		if (second_target) {
			$.make_damage($.owner, second_target, this.type, $.by_level_param(1));
			// $.change_param_percentage($.owner, second_target.strength, -$.by_level_param(2), $.by_level_param(3))
			$.apply_param_modifier(1, second_target,
				[new TypedValue(ParamType.strength, new Value(ValueType.active_ability, -$.by_level_param(2), ValueNumberType.percentage, 1)),

				new TypedValue(ParamType.endurance, new Value(ValueType.active_ability, -$.by_level_param(2), ValueNumberType.percentage, 1))]);
		}
		// $.change_param_percentage($.owner, $.single(targets).strength, -$.by_level_param(2), $.by_level_param(3))
		$.apply_param_modifier(1, $.single(targets),
			[new TypedValue(ParamType.strength, new Value(ValueType.active_ability, -$.by_level_param(2), ValueNumberType.percentage, 1)),

			new TypedValue(ParamType.endurance, new Value(ValueType.active_ability, -$.by_level_param(2), ValueNumberType.percentage, 1))]);
	},
} as ActiveNakedSkill;