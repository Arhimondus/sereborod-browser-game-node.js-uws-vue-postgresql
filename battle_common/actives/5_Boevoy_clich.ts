/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../hero.ts" />
import { Battle, ActiveNakedSkill, ActiveSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { Hero, TypedValue, ParamType, Value, ValueType, ValueNumberType } from '../hero';

/**
 * Увеличивает силу всех союзников и мародёра на %{1}.
 * @name Боевой клич
 */
export = {
	id: 5,
	class: "maroder",
	params: [[0.1, 0.14, 0.18]],

	mana_cost: [10, 18, 26],
	action_points: [3],
	cooldown: [5 * 3],

	type: SkillType.physicalSkill,
	targets: [{ unit_number: 1, target_type: SkillTargetType.no_target } as SkillTarget],
	up_level: [2, 5, 10],

	apply($, targets) {
		let owner_heroes = $.battle.heroes_alive.filter(h => h.side === $.owner.side);
		owner_heroes.forEach(h => {
			$.apply_param_modifier(1, h,
				[new TypedValue(ParamType.strength, new Value(ValueType.active_ability, $.by_level_param(1), ValueNumberType.percentage))])
		});

	},
} as ActiveNakedSkill;