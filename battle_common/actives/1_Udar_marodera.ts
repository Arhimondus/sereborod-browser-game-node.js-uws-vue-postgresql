/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../hero.ts" />

import { Battle, ActiveNakedSkill, ActiveSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { Hero } from '../hero';

/**
 * Наносит ${1} ед. урона + %{2} от силы. В случае если удар завершится смертью цели, то мародёр получит за бой золота на %{3} больше.
 * @name Удар мародёра
 */
export = {
	id: 1,
	class: "maroder",
	params: [[5, 10, 15], [0.10], [0.10]],

	mana_cost: [4],
	action_points: [1],
	cooldown: [0],
	// step_duration: [0],
	minimal_round: [0],

	type: SkillType.physicalAttack,
	targets: [{ unit_number: 1, target_type: SkillTargetType.enemy } as SkillTarget],
	up_level: [1, 5, 10],

	apply($, targets) {
		$.make_damage($.owner, $.single(targets), SkillType.physicalAttack, $.by_level_param(1) + $.by_level_param(2) * $.owner.strength.total_value);
	},
} as ActiveNakedSkill;