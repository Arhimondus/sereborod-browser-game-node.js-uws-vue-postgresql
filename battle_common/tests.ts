declare let require: any;
declare let describe: any;
declare let it: any;

const assert = require('assert');

import { Battle } from "./battle";
import C from "./common";

// h.active_skills = h._chero.active_skills.map(as => new ActiveSkill(this, require(path.resolve(glob.sync(`${__dirname}/actives/${as.id}_*.js`)[0])), h, as.level));

// h.passive_skills = h._chero.passive_skills.map(as => new PassiveSkill(this, require(path.resolve(glob.sync(`${__dirname}/passives/${as.id}_*.js`)[0])), h, as.level));

describe('1_Udar_Marodera', function () {
	describe('#test1', function () {

		let common_battle: C.Battle = {
			"id": "737820fe-989f-4a10-a330-ca8f2e6cc397",
			"status": 2,
			"heroes": [{
				"id": 36,
				"name": "Дурик",
				"class": "maroder",
				"level": 4,

				"strength": 10,
				"agility": 10,
				"intelligence": 10,
				"endurance": 10,
				"luck": 10,

				"active_skills": [{ id: 1, level: 3 }],
				"passive_skills": [{ id: 1, level: 1 }],

				"side": 1,

				"_auth": {
					"token": "34_a39fc96f-8914-4fbd-a709-005bf7ba2c0a",
					"session": "cbdf4a22-06e2-4a5b-b70c-b1bac2ca20d2"
				}
			}, {
				"id": 33,
				"name": "Лемур",
				"class": "maroder",
				"level": 4,

				"strength": 10,
				"agility": 10,
				"intelligence": 10,
				"stamina": 10,
				"luck": 10,

				"active_skills": [{ id: 1, level: 3 }],
				"passive_skills": [{ id: 1, level: 3 }],

				"side": 2,

				"_auth": {
					"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
					"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
				}
			}] as C.Hero[]
		};
		
		let battle = new Battle(common_battle);
		
		let first_hero_health = battle.heroes[0].health;
		let second_hero_health = battle.heroes[1].health;
		
		battle.heroes[0].slots = [{ skill_id: 1, targets: [[33]] }, null, null];
		battle.heroes[1].slots = [{ skill_id: 1, targets: [[36]] }, null, null];

		if (battle.heroes.every(h => h.slots != null)) {
			battle.do_all_actions();
		}

		it('здоровье первого персонажа должно быть уменьшено на величину урона способности второго персонажа', function () {
			assert.equal(first_hero_health - battle.heroes[1].active_skills[0].by_level_param(1), battle.heroes[0].health);
		});

		it('здоровье второго персонажа должно быть уменьшено на величину урона способности первого персонажа', function () {
			assert.equal(second_hero_health - battle.heroes[0].active_skills[0].by_level_param(1), battle.heroes[1].health);
		});
	})
});