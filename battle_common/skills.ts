export enum SkillType {
	physicalSkill = 1,
	physicalAttack = 2,
	physicalDamage = 3,
	/*+*/
	magicalSkill = 4,
	magicalAttack = 5,
	magicalDamage = 6,
	/*+*/
	clearDamage = 7,

	mentalAttack = 8,
	mentalDamage = 9,
	mentalSkill = 10,
	/*+*/
	mentalPurge = 11,
}

export class SkillTarget {
	unit_number: number;
	target_type: SkillTargetType;
}

export enum SkillTargetType {
	enemy,
	ally,
	both,
	ally_no_cur,
	no_target,
}