/// <reference path="common.ts" />
/// <reference path="modules.d.ts" />
/// <reference path="events.ts" />
/// <reference path="skills.ts" />

declare let require: any;
declare let __dirname: any;
// declare let DbHero: any;

const glob = require('glob'),
	path = require('path'),
	{ deprecate } = require('core-decorators');

import * as C from './common';
import { Hero, Param, Value, DeathType, ParamType, ValueType, ValueNumberType, TypedValue, ParamModifier, Modifier, ActionModifier } from './hero';
import { EventType, EventParams } from './events';
import { SkillType, SkillTarget, SkillTargetType } from './skills';

export interface Slot {
	skill_id: number;
	targets: number[][]; //[[11,20,3],[32,4]]
}

export class NakedSkill {
	id: number;
	params: number[][];
	priority: number = 1;
	initialize?(skill: ActiveSkill | PassiveSkill, hero: Hero);
	continue_action?(data: any);
}
export class ActiveNakedSkill extends NakedSkill {
	class: string;
	action_points: number[]; //стоимость очков действия
	mana_cost: number[]; //стоимость магической энергии
	cooldown: number[]; //перезарядка в этапах
	// step_duration?: number[]; //длительность в этапах
	minimal_round?: number[]; //минимальный раунд
	up_level: number[]; //уровни для повышения
	type: SkillType; //тип способности
	targets: SkillTarget[]; //список возможных типов целей
	apply(skill: ActiveSkill, targets: Hero[][]) { };
}
export class PassiveNakedSkill extends NakedSkill {
	events?: EventType[]; //список событий на которые срабатывает пассивная способность
	apply?(skill: PassiveSkill, eventType: EventType, eventParams: EventParams);
	//can_randomize: boolean;
}

export class Skill {
	naked: NakedSkill;
	level: number;
	owner?: Hero;
	battle: Battle;

	constructor(battle: Battle, naked: NakedSkill, owner: Hero = null, level: number = 1) {
		this.naked = naked;
		this.owner = owner;
		this.level = level;
		this.battle = battle;
	};
	/**
	 * Получает одиночную цель для способности
	 */
	single(targets: Hero[][]): Hero {
		return targets[0][0];
	};
	/**
	 * Получает определённую группу целей для способности, по умолчанию без второго аргумента возвращается первая
	 */
	group(targets, group_number = 1) {
		return targets[group_number - 1];
	};
	restore_health(initiator: Hero, target: Hero, skill_type: SkillType, value) {
		return 10;
	};
	by_level_param(number: number = 1): number {
		return this.naked.params[number - 1][this.level - 1] || this.naked.params[number - 1][this.naked.params[number - 1].length - 1];
	};
	make_damage(from: Hero, to: Hero, type: SkillType, rawValue: number): any {
		//ѕроверка пассивок
		//todo... 
		var skill = this.naked as ActiveNakedSkill;
		let value = new IntegerRef(rawValue);
		let dmg = new IntegerRef(0);

		switch (skill.type) {
			case SkillType.physicalAttack:
			case SkillType.physicalDamage: //EEventType.BeforeApplyDamage //EEventType.AfterApplyDamage //EEventType.
				this.battle.do_event_actions(EventType.before_taken_damage, { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
				this.battle.do_event_actions(EventType.before_taken_physical_damage, { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
				dmg = new IntegerRef(value.value * (100 - to.physical_protection.total_value) / 100);
				//if(to.abilities_passive[35]) {
				//to.abilities_passive[35].params.health -= dmg.value;
				to.health -= dmg.value;
				this.battle.do_event_actions(EventType.after_taken_damage, { target: to, initiator: from, damage: dmg, damage_type: type });
				this.battle.do_event_actions(EventType.after_taken_physical_damage, { target: to, initiator: from, damage: dmg, damage_type: type });
				//}
				break;
			case SkillType.magicalAttack:
			case SkillType.magicalDamage:
				this.battle.do_event_actions(EventType.before_taken_damage, { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
				this.battle.do_event_actions(EventType.before_taken_magical_damage, { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
				dmg = new IntegerRef(value.value * (100 - to.magical_protection.total_value) / 100);
				//if(to.abilities_passive[35]) {
				//to.abilities_passive[35].params.health -= dmg.value;
				to.health -= dmg.value;
				this.battle.do_event_actions(EventType.after_taken_damage, { target: to, initiator: from, damage: value, damage_type: type });
				this.battle.do_event_actions(EventType.after_taken_magical_damage, { target: to, initiator: from, damage: dmg, damage_type: type });
				//}
				break;
			case SkillType.clearDamage:
				this.battle.do_event_actions(EventType.before_taken_damage, { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
				this.battle.do_event_actions(EventType.before_taken_pure_damage, { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
				dmg = new IntegerRef(value.value);
				//if(to.abilities_passive[35]) {
				//to.abilities_passive[35].params.health -= dmg.value;
				to.health -= dmg.value;
				this.battle.do_event_actions(EventType.after_taken_damage, { target: to, initiator: from, damage: value, damage_type: type });
				this.battle.do_event_actions(EventType.after_taken_pure_damage, { target: to, initiator: from, damage: dmg, damage_type: type });
				//}
				break;
			default:
				break;
		}

		return { total_damage: dmg.value, success: true };
	};
	base_on_param(param_number: number, range_numbers: number[][]): number {
		let modifier = 0;
		for (var [check, set] of range_numbers.reverse()) {
			if (param_number >= check) {
				modifier = set;
				break;
			}
		}
		return modifier;
	};
	paralyze(from: Hero, to: Hero, type: SkillType, action_points_reduce: number) {

	};
	successfully(chance: number): boolean {
		return Math.random() <= chance;
	};


	apply_full_modifier(modifier_id: number, to: Hero, values: TypedValue[], continue_data) {
		new Modifier(modifier_id, this, to, values, continue_data);
	};
	apply_param_modifier(modifier_id: number, to: Hero, values: TypedValue[]) {
		new ParamModifier(modifier_id, this, to, values);
	};
	apply_action_modifier(modifier_id: number, to: Hero, continue_data) {
		new ActionModifier(modifier_id, this, to, continue_data);
	};
	apply_opposite_modifier(modifier_id: number, value: Value, elapsed_time?: number) {
		new ParamModifier(modifier_id, this, value.param.owner, [new TypedValue(value.param.type, new Value(value.type, -value.value, value.number_type, elapsed_time || value.elapsed_time))]);
	}
}

export class ActiveSkill extends Skill {
	naked: ActiveNakedSkill;

	cooldown_timer: number = 0;

	/*get id() {
		return this.naked.id;
	};
	get priority() {
		return this.naked.priority;
	};*/
	get action_points() {
		return this.naked.action_points[this.level - 1] || this.naked.action_points[this.naked.action_points.length - 1];
	};
	get mana_cost() {
		return this.naked.mana_cost[this.level - 1] || this.naked.mana_cost[this.naked.mana_cost.length - 1];
	};
	get cooldown() {
		return this.naked.cooldown[this.level - 1] || this.naked.cooldown[this.naked.cooldown.length - 1];
	};
	// get step_duration() {
	// 	return this.naked.step_duration[this.level - 1] || this.naked.step_duration[this.naked.step_duration.length - 1];
	// };
	get minimal_round() {
		return this.naked.minimal_round[this.level - 1] || this.naked.minimal_round[this.naked.minimal_round.length - 1];
	};
	get up_level() {
		return this.naked.up_level[this.level - 1] || this.naked.up_level[this.naked.up_level.length - 1];
	};
	get params() {
		return this.naked.params.map(p => p[this.level - 1] || p[p.length - 1]);
	};
	apply(targets: any) {
		this.naked.apply(this, targets);
	};

	constructor(battle: Battle, naked: ActiveNakedSkill, owner: Hero = null, level: number = 1) {
		super(battle, naked, owner, level);
		this.naked = naked;
	};

	serialize() {
		return {
			id: this.naked.id,
			priority: this.naked.priority,
			targets: this.naked.targets,

			// id: this.id,
			// priority: this.priority,
			cooldown_timer: this.cooldown_timer,
			mana_cost: this.mana_cost,
			cooldown: this.cooldown,
			action_points: this.action_points,
			// step_duration: this.step_duration,
			minimal_round: this.minimal_round,
			up_level: this.up_level,
			level: this.level,
			params: this.params,
		};
	};
}

export class IntegerRef {
	value: number;
	constructor(value: number) {
		this.value = Math.floor(value);
	};
}

export class PassiveSkill extends Skill {
	naked: PassiveNakedSkill;
	custom: boolean = false;
	d: number;

	constructor(battle: Battle, naked: PassiveNakedSkill, owner: Hero = null, level: number = 1) {
		super(battle, naked, owner, level);
		this.naked = naked;
	};
	get params() {
		return this.naked.params.map(p => p[this.level - 1] || p[p.length - 1]);
	};
	apply(eventType: EventType, eventParams: EventParams) {
		this.naked.apply(this, eventType, eventParams);
	};

	serialize() {
		return {
			id: this.naked.id,
			priority: this.naked.priority,
			params: this.params,
		};
	};
}

interface BattleSkill {
	skill: ActiveSkill,
	targets: Hero[][]; //hero.id
}

export class Battle {
	heroes: Hero[];
	current_round: number = 0;
	step: number = 1;
	battle_skills: BattleSkill[];
	is_end: boolean = false;

	constructor(battle: C.Battle) {
		this.heroes = battle.heroes.map(h => new Hero(h));
		this.heroes.forEach(h => {
			h.id = h._chero.id;
			h.name = h._chero.name;
			h.class = h._chero.class;
			h.level = h._chero.level;
			h.side = h._chero.side;

			h.max_health = Math.ceil(h._chero.endurance * 1.22); //Вычислить от пассивки
			h.max_energy = Math.ceil(h._chero.intelligence * 1.22); //Вычислить от пассивки

			h.health = h.max_health;
			h.energy = h.max_energy;
			
			h.alive = true;

			h.params = [
				new Param(h, ParamType.strength, new Value(ValueType.param, h._chero.strength)),
				new Param(h, ParamType.agility, new Value(ValueType.param, h._chero.agility)),
				new Param(h, ParamType.intelligence, new Value(ValueType.param, h._chero.intelligence)),
				new Param(h, ParamType.endurance, new Value(ValueType.param, h._chero.endurance)),
				new Param(h, ParamType.luck, new Value(ValueType.param, h._chero.luck)),
				new Param(h, ParamType.physical_protection, new Value(ValueType.param, h._chero.physical_protection || 0)),
				new Param(h, ParamType.magical_protection, new Value(ValueType.param, h._chero.magical_protection || 0)),
				new Param(h, ParamType.damage_increased, new Value(ValueType.param, h._chero.damage_increased || 0)),
				new Param(h, ParamType.protection_increased, new Value(ValueType.param, h._chero.protection_increased || 0)),
				//new Param(h, ParamType.special_buff, new Value(ValueType.param, 0)),
			];
			h.active_skills = h._chero.active_skills.map(as => new ActiveSkill(this, require(path.resolve(glob.sync(`${__dirname}/actives/${as.id}_*.js`)[0])), h, as.level));
			h.passive_skills = h._chero.passive_skills.map(as => new PassiveSkill(this, require(path.resolve(glob.sync(`${__dirname}/passives/${as.id}_*.js`)[0])), h, as.level));
			h.slots = [];
		});
		console.log(`бой #${battle.id} успешно начат`);
		this.next_round();
	};

	get heroes_alive() {
		return this.heroes.filter(h => h.alive);
	};
	get heroes_not_alive() {
		return this.heroes.filter(h => !h.alive);
	};

	to_log(message) {
		//LogMessages.Add(message);
	};

	decrease_time() {
		for (let hero of this.heroes_alive) {
			hero.params.forEach(param => {
				param.values.filter(v => v.type == ValueType.active_ability && v.elapsed_time >= 1).forEach(v => v.elapsed_time--);
				param.values = param.values.filter(w => w.type == ValueType.active_ability && w.elapsed_time > 0);
			});

			hero.active_skills.filter(a => a.cooldown_timer > 0).forEach(aa => aa.cooldown_timer--);

			hero.passive_skills.filter(p => p.custom && p.d > 0).forEach(ap => ap.d--);
			hero.passive_skills = hero.passive_skills.filter(p => !(p.custom && p.d == 0));
		}
	};

	check_death() {
		this.heroes.filter(h => h.death_type == DeathType.normal && h.health <= 0).forEach(hero => {
			hero.slots = null;
			hero.alive = false;
		});
	};

	do_all_actions() {
		for (var step = 0; step < 3; step++) {
			this.step = step;
			this.do_continue_action_skills();
			this.decrease_time();
			this.battle_skills = [...this.get_all_skills(step)];
			for (var skill of this.battle_skills.sort(o => o.skill.naked.priority).reverse()) {
				skill.skill.apply(skill.targets);
				skill.skill.cooldown_timer = skill.skill.cooldown;
			}
			this.check_death();
			if (this.check_end())
				return;
		}

		for (var hero of this.heroes_alive) {
			hero.energy += 3;
			if (hero.energy > hero.max_energy)
				hero.energy = hero.max_energy;
		}

		this.next_round();
		return this;
	};

	check_end(): boolean {
		if (this.heroes_alive.length <= 1) {
			this.is_end = true;
			if (this.heroes_alive.length == 1) {
				var winSide = this.heroes.filter(h => h.side == this.heroes_alive[0].side);
				var loseSide = this.heroes.filter(h => h.side != this.heroes_alive[0].side);
				for (var hero of winSide) {
					// DbHero.instant_add_experience(hero._chero.id, 50);
					this.to_log("<span class='hero'>" + hero._chero.name + "</span> +50 опыта");
				}
				for (var hero of loseSide) {
					// DbHero.instant_add_experience(hero._chero.id, 20);
					this.to_log("<span class='hero'>" + hero._chero.name + "</span> +20 опыта");
				}
			}
			else {
				for (var hero of this.heroes) {
					// DbHero.instant_add_experience(hero._chero.id, 33);
					this.to_log("<span class='hero'>" + hero._chero.name + "</span> +33 опыта");
				}
			}
			return true;
		}
		return false;
	};

	next_round() {
		for (var gHero of this.heroes_alive)
			gHero.slots = null;
		this.current_round++;
		console.log(`стартуем раунд ${this.current_round}`);
	};

	do_continue_action_skills() {
		this.heroes_alive.forEach(h => {
			h.modifiers
				.filter(m => m.continue_data)
				.forEach(m => m.skill.naked.continue_action(m.continue_data));
		});
	};

	//+/delegate void SkillAction (),

	/*+class SkillData
	{
		List<GHero> Targets,
		Skill Skill,
	}*/

	/*bool*/ check_targets(/*SkillData*/ sd) {
		if (sd.Targets != null) {
			for (var hero of sd.Targets) {
				if (hero == null)
					return false;
				if (!sd.Skill.Owner.Battle.Heroes.Contains(hero))
					return false;
				switch (sd.Skill.TargetType) {
					case SkillTargetType.enemy:
						if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side != sd.Skill.Owner.Side).Contains(hero))
							return false;
						break;
					case SkillTargetType.ally:
						if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
							return false;
						break;
					case SkillTargetType.ally_no_cur:
						if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
							return false;
						if (sd.Skill.Owner == hero)
							return false;
						break;
				}
			}
		}

		return true;
	};

	*get_all_skills(step) {
		for (var hero of this.heroes_alive) {
			if (hero.slots.length < 3) continue;

			var slot = hero.slots[step];
			if (slot == null || slot.skill_id == 0) continue;
			var skill = hero.active_skills.find(a => a.naked.id == slot.skill_id);

			if (skill.action_points == 2) {
				if (step == 2)
					continue;
				if (step == 1)
					hero.slots[2] = null;
				if (step == 0)
					hero.slots[1] = null;
			}

			if (skill.action_points == 3) {
				if (step == 0) {
					hero.slots[1] = null;
					hero.slots[2] = null;
				}
				else
					continue;
			}

			if (skill.mana_cost <= hero.energy)
				hero.energy -= skill.mana_cost;
			else
				continue;

			if (skill.cooldown_timer > 0)
				continue;

			if (skill != null) {
				let sd = {
					targets: slot.targets.map(group => group.map(target => this.heroes.find(h => h.id === target) as Hero)),
					skill: skill
				};
				// if (this.check_targets(sd)) //Dangerous TODO!!!
				yield sd;
			}
		}
	};

	do_event_actions(event_type: EventType, event_params: EventParams) {
		for (var hero of this.heroes) {
			for (var pa of hero.passive_skills.filter(p => p.naked.events.some(ev => ev == event_type))) {
				pa.apply(event_type, event_params);
			}
		}
	};

	serialize() {
		return {
			current_round: this.current_round,
			is_end: this.is_end,
			heroes: this.heroes.map(m => m.serialize()),
		};
	};
}