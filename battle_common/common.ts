export interface Battle {
	id: string;
	status: number;
	heroes: Hero[];
}

export interface Hero {
	id: number;
	name: string;
	class: string;
	level: number;

	strength: number;
	agility: number;
	intelligence: number;
	endurance: number;
	luck: number;

	physical_protection?: number;
	magical_protection?: number;
	damage_increased?: number;
	protection_increased?: number;

	active_skills: HeroSkill[];
	passive_skills: HeroSkill[];

	side: number;

	_auth: Auth;
}

export interface HeroSkill {
	id: number;
	level: number;
}

export interface Auth {
	token: string;
	session: string;
}