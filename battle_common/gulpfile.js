const gulp = require('gulp'),
	tsc = require('gulp-typescript'),
	clean = require('gulp-rm');
 
gulp.task('compile', function(){
	gulp.src(['**/*.ts', '!node_modules/**'])
		.pipe(tsc({
			target: 'ES2017',
			module: 'commonjs',
			experimentalDecorators: true,
			allowSyntheticDefaultImports: true,
		}))
		.pipe(gulp.dest(''));
});

gulp.task('clean', function(){
	gulp.src(['**/*.js', '!node_modules/**', '!gulpfile.js'])
		.pipe(clean());
});

gulp.task('watch', function() {
	gulp.start('compile');
	gulp.watch(['**/*.ts', '!node_modules/**'], ['compile']);
});