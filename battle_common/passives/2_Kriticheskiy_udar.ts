/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../events.ts" />
import { Battle, PassiveNakedSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { EventType } from '../events';

/**
 * Вероятность {0}% нанести в два раза больше урона физическими атаками.
 * @name Критический удар
 */
export = {
	id: 2,
	// name: "Критический удар",
	// description: "Вероятность {0}% нанести в два раза больше урона физическими атаками.",
	params: [[0.10, 0.15, 0.20]],
	
	events: [EventType.after_taken_damage],

	apply($, eventType, eventParams) {
        if($.successfully($.by_level_param()))
            eventParams.supposed_damage.value *= 2;
	},
} as PassiveNakedSkill;