/// <reference path="../battle.ts" />
/// <reference path="../skills.ts" />
/// <reference path="../events.ts" />
import { Battle, PassiveNakedSkill } from '../battle';
import { SkillType, SkillTargetType, SkillTarget } from '../skills';
import { EventType } from '../events';

/**
 * Восстанавливает %{1} от нанесённого урона физическими атаками в здоровье.
 * @name Вампиризм
 */
export = {
	id: 1,
	// name: "Вампиризм",
	// description: "Восстанавливает %{1} от нанесённого урона физическими атаками в здоровье.",
	params: [[0.075, 0.150, 0.225]],

	events: [EventType.after_taken_damage],

	apply($, eventType, eventParams) {
		$.restore_health($.owner, $.owner, SkillType.physicalSkill,  eventParams.damage);
	},
} as PassiveNakedSkill;