/// <reference path="battle.ts" />
/// <reference path="hero.ts" />
import { ActiveSkill, PassiveSkill, Slot, IntegerRef, Skill } from './battle';
import { Hero } from './hero';

export enum EventType {
	before_hero_death, // ДоСмертиПерсонажа
	after_hero_death, // ПослеСмертиПерсонажа
	// 
	before_taken_damage, // ДоНанесенияУронаПерсонажу
	before_taken_physical_damage,// ДоНанесенияФизическогоУронаПерсонажу
	before_taken_magical_damage,// ДоНанесенияМагическогоУронаПерсонажу
	before_taken_pure_damage,// ДоНанесенияЧистогоУронаПерсонажу
	// 
	after_taken_damage, // ПослеНанесенияУронаПерсонажу
	after_taken_physical_damage, // ПослеНанесенияФизическогоУронаПерсонажу
	after_taken_magical_damage, // ПослеНанесенияМагическогоУронаПерсонажу
	after_taken_pure_damage, // ПослеНанесенияЧистогоУронаПерсонажу
	// 
	after_sign_in, // ПриВходе
	// 
	at_the_start_of_game, // НачалоБоя
	at_the_end_of_game, // КонецБоя
	// 
	before_start_round, // ДоНачалаРаунда
	after_start_round, // ПослеНачалаРаунда
	// 
	before_end_round, // ДоКонцаРаунда
	after_end_round, // ПослеКонцаРаунда
	// 
	before_start_step, // ДоНачалаЭтапа
	after_start_step, // ПослеНачалаЭтапа
	// 
	before_end_step, // ДоКонцаЭтапа
	after_end_step, // ПослеКонцаЭтапа
}

export interface EventParams {
	target?: Hero;
	initiator?: Hero;
	skill?: Skill;
	damage?: IntegerRef;
	damage_type?: any;
	supposed_damage?: IntegerRef;
}