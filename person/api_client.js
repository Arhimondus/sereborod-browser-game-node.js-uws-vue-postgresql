const bluebird = require('bluebird'),
	bcrypt = require('bcryptjs'),
	shortid = require('shortid'),
	uuid = require('uuid/v4'),
	captchaGenerator = require('trek-captcha'),
	redis = require('redis'),
	client = bluebird.promisifyAll(redis.createClient());
	
module.exports.setup = function (app, db, sereborod) {
	async function current_user(req) {
		let token = req.query.token || null;
		if (token == null)
			throw new Error("Отсутствует токен");
		let [uid, _] = (await client.getAsync(token)).split(",").map((v) => v - 0);
		if (uid == null)
			throw new Error("Неверный токен");
		return uid;
	}
	async function current_hero(req) {
		let token = req.query.token || null;
		if (token == null)
			throw new Error("Отсутствует токен");
		let [uid, hid] = (await client.getAsync(token)).split(",").map((v) => v - 0);
		if (uid == null)
			throw new Error("Неверный токен");
		if (!hid) throw new Error("Герой не был выбран");
		return hid;
	}

	function hero_info(id, name) {
		this.id = id;
		this.name = name;
		this._rawDBType = true;
		this.formatDBType = function () {
			return pgp.as.format('($1, $2)::hero_info', [this.id, this.name]);
		}
	}

	app.get('/test6', async (req, res) => {
		let data = await sereborod.arena('hero/list');
		console.log(data);
		res.send('kk');
	});

	/**
	* @swagger
	* definitions:
	*   Target:
	*     required:
	*       - id
	*       - fake
	*       - dang
	*       - votes
	*     properties:
	*       id:
	*         type: string
	*       fake:
	*         type: integer
	*       dang:
	*         type: integer
	*       votes:
	*         type: integer
	*/

	/**
	* @swagger
	* /register:
	*   post:
	*     description: Регистрация
	*     parameters:
	*     - name: name
	*       description: Имя
	*       in: formData
	*       required: true
	*       type: string
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: email
	*       description: Почта
	*       in: formData
	*       required: true
	*       type: string
	*     - name: identifier
	*       description: Идентификатор сессии
	*       in: formData
	*       required: true
	*       type: string
	*     - name: token
	*       description: Токен безопасности
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Успешная регистрация		 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/register", async function (req, res) {
		try {
			let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

			let identifier = req.body.identifier;
			let registerToken = req.body.token;

			let trueToken = await client.getAsync(`sbd:register:${ip}:${identifier}`);
			if (trueToken == null || registerToken != trueToken)
				return res.status(400).send("Неверный токен безопасности!");
			await client.delAsync(`sbd:register:${ip}:${identifier}`);

			let raw_password = req.body.password;
			let name = req.body.name;
			let email = req.body.email;
			let login = req.body.login;

			let password = bcrypt.hashSync(raw_password, 10);

			let code = shortid.generate();

			let x = db.one('INSERT INTO accounts(name, email, login, password, code) VALUES(${name}, ${email}, ${login}, ${password}, ${code}) RETURNING id', {
				name, email, login, password, code
			}).then(async data => {
				let uid = data.id;
				let token = uid + "_" + uuid();
				await client.setAsync(token, data.id + ",0");
				//await client.expire(token, 300);
				return res.json({ uid, name, token, });
			}).catch(error => {
				return res.status(400).json(error);
			});
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /register/captcha:
	*   get:
	*     description: Запрос идентификатора и капчи для регистрации 
	*     responses:
	*       200:
	*         description: Получена base64 капча и идентификатор	 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/register/captcha", async function (req, res) {
		try {
			let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

			let keys = await client.scanAsync('0', 'MATCH', `sbd:register:${ip}:*`, 'COUNT', '200');
			if (keys[1].length >= 100)
				return res.status(423).send("Превышен лимит запросов для IP адреса");

			let identifier = shortid.generate();
			let { token, buffer } = await captchaGenerator();
			await client.setAsync(`sbd:register:${ip}:${identifier}`, token, 'EX', 240);
			let captcha = buffer.toString('base64');

			res.json({ identifier, captcha });
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /login:
	*   post:
	*     description: Авторизация
	*     parameters:
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*       format: password
	*     responses:
	*       200:
	*         description: Успешная авторизация	(возвращает строковую сессию)
	*       404:
	*         description: Пользователь не найден
	*       401:
	*         description: Неверный пароль
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/login", function (req, res) {
		try {
			let login = req.body.login;
			let password = req.body.password;

			db.one('SELECT * FROM accounts WHERE login = ${login} LIMIT 1', {
				login,
			}).then(async account => {
				if (!bcrypt.compareSync(password, account.password))
					return res.status(401).send("Неверный пароль");

				let token = account.id + "_" + uuid();
				await client.setAsync(token, account.id + ",0");
				//await client.expire(token, 300);
				//Добавить просто этот токен в саму базу постгрес, что такой токен есть и связан с этим пользователем
				res.json({
					token: token,
					name: account.name,
					uid: account.id,
					heroes_limit: account.heroes_limit,
				});
			}).catch(error => {
				//res.status(400).json(error);
				res.status(404).send("Пользователь не найден");
			});
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	//pgp.pg.types.setTypeParser(24922, function() {});
	app.post("/@/battle/status/open", async function (req, res) {
		try {
			let battles = await db.query('SELECT id, status, mode, params, heroes FROM battles WHERE status = 1');
			res.json(battles);
		} catch (ex) {
			console.log(ex);
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	/*app.post("/battle/create", async function(req, res) {
		try {
			let battles = (await client.scanAsync('0', 'MATCH', `sbd:open_battles:*`, 'COUNT', '20'))[1];
			res.send(JSON.stringify(battles));
		} catch(ex) {
			res.send(400, ex);
		}
	});*/

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/list", async function (req, res) {
		try {
			let uid = await current_user(req);
			let heroes = await db.query('SELECT id, name, class, level FROM heroes WHERE account_id = ${uid}', {
				uid
			});
			res.json(heroes);
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Создать нового героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/create", async function (req, res) {
		try {
			let token = req.query.token || null;
			if (token == null)
				throw new Error("Отстутствует токен");
			let [uid, _] = (await client.getAsync(token)).split(",").map((v) => v - 0);
			if (uid == null)
				throw new Error("Неверный токен");
			let hero_id = (await db.one('INSERT INTO heroes(account_id, class, name) VALUES(${account_id}, ${class}, ${name}) RETURNING id', {
				account_id: uid,
				class: req.body.class,
				name: req.body.name,
			})).id;
			res.json(hero_id);
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /@/hero/select:
	*   post:
	*     description: Выбрать героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/select", async function (req, res) {
		try {
			let token = req.query.token || null;
			if (token == null)
				throw new Error("Отстутствует токен");
			let [uid, _] = (await client.getAsync(token)).split(",").map((v) => v - 0);
			if (uid == null)
				throw new Error("Неверный токен");
			let hero = await db.one('SELECT * FROM heroes WHERE account_id = ${account_id} AND id = ${id} LIMIT 1', {
				account_id: uid,
				id: req.body.id,
			});
			if (hero.current_battle)
				hero.battle = await db.one('SELECT id, status FROM battles WHERE id = ${current_battle} AND (status = 1 OR status = 2)', {
					current_battle: hero.current_battle,
				});
			else {
				//Пытаемся найти героя в уже существующем бою!
				let battles = await db.query(`SELECT id, status FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]' LIMIT 1`, {
					id: hero.id,
				});
				if (battles.length > 0) {
					hero.battle = battles[0];
					//Нужно ли проставлять current_battle?
				}
			}
			await client.setAsync(token, uid + "," + hero.id);
			//console.log('yyy');
			let data = await sereborod.arena('hero/assign', { hero, token });
			//console.log(data);
			//console.log('xxx');
			Object.assign(hero, { _meta: { location: 'arena' }});
			
			res.json(hero);
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /@/hero/select:
	*   post:
	*     description: Выход
	*/
	app.post("/@/logout", async function (req, res) {
		try {
			let token = req.query.token || null;
			if (token == null)
				throw new Error("Отстутствует токен");
			await client.delAsync(`sbd:tokens:${token}`);

			res.json('ok');
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Просмотр героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/view", async function (req, res) {
		try {
			let token = req.query.token || null;
			if (token == null)
				throw new Error("Отстутствует токен");
			let [uid, hid] = (await client.getAsync(token)).split(",").map((v) => v - 0);
			if (uid == null)
				throw new Error("Неверный токен");
			if (hid == null)
				throw new Error("Не выбран герой");
			let hero = (await db.one('SELECT id FROM heroes WHERE account_id = ${account_id} AND id = ${id} LIMIT 1', {
				account_id: uid,
				id: req.body.id,
			}));
			if (hero.length != 1)
				throw new Error("Неверный герой?");
			res.status(200).json(hero);
		} catch (ex) {
			res.status(400).send(ex);
		}
	});

	app.post("/@/status", async function (req, res) {
		async function status(server) {
			try {
				return await sereborod[server]('context');
			} catch (err) {
				console.log(err);
				return { server: { server_name: server, status: 0 } };
			}
		}

		res.json(await Promise.all(['arena', 'battle'].map(server => status(server))));
	});
};