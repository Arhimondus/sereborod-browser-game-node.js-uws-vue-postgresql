﻿/*var loki = require('lokijs');
var db = new loki('b1s.json');
var collection = db.addCollection('abc', { indices:['email'], autoupdate: true });
collection.insert({ b: 5, email: 'afasfs', array: [1, 2, 3] });
var g = collection.get(1);
console.log(g);
g.b = 6;
g.array.push(666);
g.yogurt = 500;
var c = collection.get(1);
console.log(c);
return;*/
const express = require('express'),
	bodyParser = require('body-parser'),
	cors = require('cors'),
	// swagger = require('restify-swagger-jsdoc'),
	app = express(),
	http = require('http').Server(app),
	// io = require('socket.io')(http),
	port = require('yargs').argv.port || 4003,
	pgp = require('pg-promise')(),
	cfg = require('./server.cfg.js'),
	db = pgp(cfg.db),
	sereborod = require('server_link')(require('./config.local.json'), 'person');
	//circularJsonParser = require('../base_server/circular-json-parser.js');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// app.use(circularJsonParser());
// app.use(bodyParser.text({ type: 'application/json' }));
// app.use(circularJsonParser());

// app.use(express.static('client'));
 
require('./api_client').setup(app, db, sereborod);
require('./api_server').setup(app, db, sereborod);

// swagger.createSwaggerPage({
//     title: 'API documentation', // Page title (required) 
//     version: '1.0.0', // Server version (required) 
//     server: app, // Restify server instance created with restify.createServer() 
//     path: '/docs/swagger', // Public url where the swagger page will be available 
//     apis: [ `${__dirname}/api.js` ], // Path to the API docs 
//     //routePrefix: null // prefix to add for all routes (optional) 
// });

http.listen(port, function () {
	console.log(`started on port ${port}`);
});