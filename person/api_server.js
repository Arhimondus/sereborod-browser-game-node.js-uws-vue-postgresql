const bluebird = require('bluebird'),
	bcrypt = require('bcryptjs'),
	shortid = require('shortid'),
	uuid = require('uuid/v4'),
	captchaGenerator = require('trek-captcha'),
	redis = require('redis'),
	client = bluebird.promisifyAll(redis.createClient());

module.exports.setup = function (app, db, sereborod) {
	//Далее судя по всему чисто серверные методы, их нужно принимать только от доверенных серверов
	app.post("/~/hero/add_experience", async function (req, res) {
		let hero_id = req.query.hero_id;
		let experience = req.query.experience;
		let new_experience = await db.one('UPDATE heroes SET experience = experience + $<experience> WHERE id = $<hero_id> RETURNING experience', {
			experience,
			hero_id,
		});
		res.json(new_experience);
	});

	/**
	* @swagger
	* /$/hero/assign/notify:
	*   post:
	*     description: Уведомления головного person сервера от дочернего о том, что игрок успешно присоединился к серверу (в случае если передача местоположения идёт между дочерними)
	*/
	app.post("/~/hero/assign/notify", async function (req, res) {
		res.json('ok');
	});
	
	/**
	* @swagger
	* /$/hero/assign/notify:
	*   post:
	*     description: Уведомления головного person сервера от дочернего о том, что игрок успешно присоединился к серверу (в случае если передача местоположения идёт между дочерними)
	*/
	app.post("/~/heroes/multiple", async function (req, res) {
		let heroes = req.body;
		let $heroes = await db.query('SELECT * FROM heroes WHERE id IN ($1:csv)', [heroes]);
		res.json($heroes);
	});
};