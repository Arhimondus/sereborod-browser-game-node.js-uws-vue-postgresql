﻿var gulp = require('gulp'),
	ts = require('gulp-typescript'),
	concat = require('gulp-concat'),
	replace = require('gulp-replace'),
	insert = require('gulp-insert');
	
gulp.task('default', function() {
	return gulp.src([
		"common/_*.ts",
		"common/Skill.ts",
		"common/Hero.ts",
		"common/Battle.ts",
		"common/NakedSkill.ts",
		"common/Skill.ts",
		"common/data/*.ts"
	])
	.pipe(ts({ target: 'esnext', emitBOM: true, removeComments: true }))
	.pipe(replace("\n", "\r\n"))
	.pipe(replace("var Sereborod;\r\n(function (Sereborod) {\r\n",""))
	.pipe(replace("})(Sereborod || (Sereborod = {}));\r\n",""))	
	.pipe(replace("extends Sereborod.","extends "))	
	.pipe(concat("sereborod.js"))
	.pipe(replace(/    /g, "\t")) //Фикс 4 пробелов
	.pipe(replace(/\\u[0-9A-F]+/g, function(match) { //Фикс неправильной конвертации Unicode в идентификаторах перечислений
		return unescape('%u' + match.slice(2));
	}))
	.pipe(replace(/\t+;/g, ""))
	.pipe(insert.prepend(`var pgp = require('pg-promise')();
//var cfg = require('./server.cfg.js');
//var db = pgp(cfg.db);
var Sereborod;
(function (Sereborod) {`))
	.pipe(insert.append("})(Sereborod || (Sereborod = {}));\r\nmodule.exports = Sereborod;"))
	.pipe(gulp.dest(''));
});

gulp.task('watch', ['default'], function() {
	gulp.watch('common/*.ts', ['default']);
});