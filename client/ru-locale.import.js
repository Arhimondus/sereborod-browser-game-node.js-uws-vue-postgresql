const glob = require('glob'),
	path = require('path'),
	fs = require('fs');

function get_skill_info(p) {
	let id = p.match(/\d+/g)[0] - 0;
	let [description, name] = fs.readFileSync(path.resolve(p), 'utf-8').match(/(?=\* ).+/g);
	
	description = description.replace('* ', '');
	name = name.replace('* @name ', '');
	
	return { id, name, description };
}

let ruLocale = {
	active_skills: glob.sync('../battle_common/actives/*.ts').map(p => get_skill_info(p)),
	passive_skills: glob.sync('../battle_common/passives/*.ts').map(p => get_skill_info(p))
};

fs.writeFileSync('src/assets/ru-locale.js', 'export default ' + JSON.stringify(ruLocale) + ';');

console.log(`writed ${ruLocale.active_skills.length} active skills`);
console.log(`writed ${ruLocale.passive_skills.length} active skills`);