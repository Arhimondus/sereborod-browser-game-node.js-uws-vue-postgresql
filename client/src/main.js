// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* global store, sereborod, application */
global.application = new Vue({
	data: {
		token: store.get('token'),
		hero: store.get('hero'),
		login: store.get('login'),
		uid: store.get('uid'),
		name: store.get('name'),
		heroes_limit: store.get('heroes_limit'),
	},
	watch: {
		token(token) {
			if(!token) store.remove('token');
			else store.set('token', token);
		},
		hero(hero) {
			if(!hero) store.remove('hero');
			else store.set('hero', hero);
		},
		login(login) {
			if(!login) store.remove('login');
			else store.set('login', login);
		},
		uid(uid) {
			if(!uid) store.remove('uid');
			else store.set('uid', uid);
		},
		name(name) {
			if(!name) store.remove('name');
			else store.set('name', name);
		},
		heroes_limit(heroes_limit) {
			if(!heroes_limit) store.remove('heroes_limit');
			else store.set('heroes_limit', heroes_limit);
		},
	},
	methods: {
		quit() {
			this.token = null;
			this.hero = null;
			this.login = null;
			this.uid = null;
			this.name = null;
			this.heroes_limit = null;
		},
	},
	router,
	template: '<App/>',
	components: { App }
});

global.application.$mount('#app');
