export default {
	lvl_description(level, naked_skill) {
		let lvl_param = naked_skill.params[level];
		return naked_skill.description.replace(/([$%]){([1-9])}/g, (_, type, param) => {
			if(type === '$') return lvl_param[param] || lvl_param.slice(-1)[0];
			else return (lvl_param[param] || lvl_param.slice(-1)[0]) * 100;
		});
	},
	skill_description({ params }, raw_description) {
		return raw_description.replace(/([$%]){([1-9])}/g, (_, type, param) => {
			let result = type === '$' ? (params[param-1] || params.slice(-1)[0]) : ((params[param-1] || params.slice(-1)[0]) * 100 + '%');
			return `<span class="param">${result}</span>`;
		});
	},
}