/* global application */
import CircularJSON from 'circular-json';

export default function(config, request_server = 'universal') {
		let static_module = (server, method, params) => {
		let token = global.application.token;
		let host = config[server].host;
		let port = config[server].port || 80;
		let ssl = config[server].ssl;
		console.log(token);
		console.log(host);
		console.log(port);
		console.log(ssl);
		return new Promise((resolve, reject) => {
			$.ajax({
				url: `http${ssl?'s':''}://${host}:${port}/@/${method}?token=${token}&server=${request_server}`,
				method: 'POST',
				contentType: 'application/json',
				dataType: 'text',
				data: JSON.stringify(params),
			}).then((res) => {
				resolve(CircularJSON.parse(res));
			}).fail(err => {
				reject(JSON.stringify(err));
			});
		});
	};

	let dynamic_module = (server, d_port) => {
		let token = global.application.token;
		let host = config[server].host;
		let port = d_port || config[server].port || 80;
		let ssl = config[server].ssl;
		
		let ws = new WebSocket(`ws${ssl?'s':''}://${host}:${port}/?token=${token}`);

		ws.onerror = function (error) {
			console.log(`error from ${host} server ${error}`);
		};
		ws.onclose = function () {
			console.log(`disconnected from ${host} server`);
		};

		ws.onmessage = function (msg) {
			//console.log(JSON.stringify(msg));
			if (!msg.data) return;
			let { event, data, error } = CircularJSON.parse(msg.data);
			ws.events.filter(e => e.event == event).forEach(e => e.callback(data, error));
		};

		ws.events = [];

		let ws_wrapper = {
			on(event, callback) {
				let ev_object = { event, callback };
				ws.events.push(ev_object);
				return ev_object;
			},
			emit(event, data, timeout = 8000) {
				return new Promise((resolve, reject) => {
					let ev = this.on(event + '/return', (data, error) => {
						clearTimeout(ev.timer);
						if(error) reject(error);
						else resolve(data);
					});
					ev.timer = setTimeout(() => {
						reject('Timeout error');
					}, timeout);
					ws.send(JSON.stringify({ event, data }), (err) => {
						if (err) reject(err);
					});
				});
			}
		};
		return new Promise((resolve) => {
			ws.onopen = function () {
				console.log(`connected to ${server} server SUCCESSFULLY`);
				resolve(ws_wrapper);
			};
		});
	};
	
	let server_links = { _config: config };
	for(let server_name in config) {
		if(config[server_name].type === 'static') {
			server_links[server_name] = (method, params) => static_module(server_name, method, params);
		}
		else if(config[server_name].type === 'dynamic') {
			server_links[server_name] = (port) => dynamic_module(server_name, port);
		}
	}
	return server_links;
}