import Vue from 'vue';
import Router from 'vue-router';

import arena from '@/pages/arena';
import index from '@/pages/index';
import login from '@/pages/login';
import logout from '@/pages/logout';
import register from '@/pages/register';
import status from '@/pages/status';
import hero_create from '@/pages/hero_create';
import hero_list from '@/pages/hero_list';
import game from '@/pages/game';
import arena_fast_start from '@/pages/arena_fast_start';

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/arena',
			name: 'Arena',
			component: arena
		},
		{
			path: '/',
			name: 'Index',
			component: index
		},
		{
			path: '/login',
			name: 'Login',
			component: login
		},
		{
			path: '/logout',
			name: 'Logout',
			component: logout
		},
		{
			path: '/register',
			name: 'Register',
			component: register
		},
		{
			path: '/status',
			name: 'Status',
			component: status
		},
		{
			path: '/hero/create',
			name: 'Hero Create',
			component: hero_create
		},
		{
			path: '/hero/list',
			name: 'Hero List',
			component: hero_list
		},
		{
			path: '/game/:port',
			name: 'Game',
			component: game
		},
		{
			path: '/arena/fast/start',
			name: 'Arena Fast Start',
			component: arena_fast_start
		}
	]
});
