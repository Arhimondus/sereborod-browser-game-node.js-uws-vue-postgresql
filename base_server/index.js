module.exports = base_server;

function base_server(server_name, default_port, server_ev, client_ev, config, context, save_load_when_exit = true) {
	const argv = require('yargs').argv,
		bodyParser = require('body-parser'),
		app = require('express')(),
		port = argv.port || default_port,
		url = require('url'),
		server = require('http').createServer(app),
		uuid = require('uuid'),
		io = new (process.arch == 'x64' ? require('uws').Server : require('ws').Server)({ port, server }),
		serialijse = require('serialijse'),
		RSON = require('rson'),
		fs = require('fs'),
		nodeCleanup = require('node-cleanup'),
		circularJsonParser = require('./circular-json-parser.js'),
		CircularJSON = require('circular-json');
		
	if(save_load_when_exit)
		if(!fs.existsSync(__dirname + '/.data')) {
			context.heroes = context.heroes || [];
		} else {
			let deserialized = CircularJSON.parse(fs.readFileSync(__dirname + '/.data', 'utf8'));
			delete deserialized.io;
			delete deserialized.servers;
			delete deserialized.server;
			Object.assign(context, deserialized);
		}
	
	context.server = {
		start_time: new Date(),
		server_name,
		port,
		get online_heroes() {
			return context.heroes.length;
		},
		status: 1,
	};
	context.io = {
		broadcast(event, data) {
			io.broadcast(JSON.stringify({ event, data }));
		},
	};
	context.servers = require('server_link')(config, server_name.toLocaleLowerCase());
	
	app.use((req, res, next) => {
		let token = req.query.token;
		let server = req.query.server;
		if (!token || !server) return res.status(403).send('Forbidden');

		if (!config[server] || config[server].token != token) {
			console.log(`неопознанное соединение от сервера ${server}`);
			return res.status(400).send('Bad Request');
		}

		next();
	});

	//app.use(bodyParser.urlencoded({ extended: false }));
	//app.use(bodyParser.urlencoded({ extended: false }));
	//app.use(bodyParser.json());
	app.use(bodyParser.text({ type: 'application/json' }));
	app.use(circularJsonParser());

	app.post('/~/status', (req, res) => {
		res.json(context.server);
	});
	
	app.post('/~/context', (req, res) => {
		res.json(context);
	});

	for (let event_name in server_ev) {
		app.post('/~/' + event_name, async (req, res) => {
			try {
				res.json(await server_ev[event_name](CircularJSON.parse(req.body), context));
			} catch (err) {
				console.error(error);
				res.status(400).send(err.message);
			}
		});
	}

	io.on('connection', function (socket, req) {
		let query = url.parse(process.arch == 'x64' ? socket.upgradeReq.url : req.url, true).query;

		let hero = context.heroes.find(hero => hero._auth.token == query.token);
		if (!hero) {
			console.log(`неудачная попытка подключения к серверу`);
			return socket.close();
		} else if (hero.socket) {
			console.log(`отсоединяем героя [${hero.name}] от сессии [${hero.session}]`);
			hero.socket.close();
		}

		hero._auth.socket = socket;
		hero._auth.socket.broadcast = (event, data) => {
			context.heroes.forEach(h => {
				if(h.id !== hero.id)
					h._auth.socket.send(JSON.stringify({ event, data }));
			});
		}
		hero._auth.token = query.token;
		hero._auth.session = uuid();

		socket.on('message', async (raw_data) => {
			try { 
				var { event, data } = JSON.parse(raw_data);
				if (!event) return;
				if (!data) data = null;
				let hero = context.heroes.find(f => f._auth.socket == socket);
				let result = await client_ev[event]({ hero, data, socket }, context);
				socket.send(CircularJSON.stringify({ event: event + '/return', data: result }));
			} catch (error) {
				console.error(error);
				socket.send(JSON.stringify({ event: event + '/return', error: error.message }));
			}
		});

		console.log(`герой [${hero.name}] присоединился с сессией [${hero._auth.session}]`);
	});

	if(save_load_when_exit)
		nodeCleanup((exitCode, signal) => {
			console.log('завершение работы сервера');
			let serialized = CircularJSON.stringify(context);
			fs.writeFileSync('.data', serialized);
			console.log('сервер завершён успешно');
		});
	
	console.log(`успешный старт на порту ${port}`);
}