module.exports = circularJsonParser;

function circularJsonParser() {
	const CircularJSON = require('circular-json');

	return function (req, res, next) {
		res.json = (data) => {
			res.setHeader('Content-Type', 'application/json');
			res.send(CircularJSON.stringify(data));
		};
		next();
	};
}