module.exports = server_link;

function server_link(config, request_server = 'universal') {
	const CircularJSON = require('circular-json'),
		rp = require('request-promise-native');

	let static_module = (server, method, params) => {
		let token = config[server].token;
		let host = config[server].host;
		let port = config[server].port || 80;
		let ssl = config[server].ssl;
		process.stdout.write(`REQUEST '${method}' BY {${request_server}} TO {${server}} ... `);
		return new Promise((resolve, reject) => {
			rp({ 
				uri: `http${ssl ? 's' : ''}://${host}:${port}/~/${method}`,
				qs: {
					token,
					server: request_server,
				},
				body: CircularJSON.stringify(params),
				method: 'post',
				headers: {
					'content-type': 'application/json'
				},
				transform: (response) => CircularJSON.parse(response),
			}).then(response => {
				process.stdout.write('[OK]');
				resolve(response);
			}).catch(error => {
				process.stdout.write('[FAIL]')
				reject(error);
			});
		});
	};

	server_links = {};
	for (let server_name in config) {
		server_links[server_name] = (method, params) => static_module(server_name, method, params);
	}
	return server_links;
}