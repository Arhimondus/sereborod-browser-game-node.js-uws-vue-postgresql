﻿const argv = require('yargs').argv,
	bc = require('../battle_common/battle.js');

let parsed_data = JSON.parse(Buffer.from(argv.data, 'base64').toString('utf8'));

const context = {
	battle: new bc.Battle(parsed_data),
};

context.heroes = parsed_data.heroes;

require('base_server')('BATTLE', 4005, require('./battle_serverIO.js'), require('./battle_clientIO.js'), require('./config.local.json'), context, false);

