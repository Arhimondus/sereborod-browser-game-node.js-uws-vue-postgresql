const argv = require('yargs').argv,
	bc = require('../battle_common/battle.js');

const context = {};

context.heroes = [{
	"id": 36,
	"name": "Дурик",
	"class": "maroder",

	"health": 40,
	"max_health": 120,
	"energy": 100,
	"max_energy": 100,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,
	
	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 1 }],

	"side": 1,

	"_auth": {
		"token": "34_a39fc96f-8914-4fbd-a709-005bf7ba2c0a",
		"session": "cbdf4a22-06e2-4a5b-b70c-b1bac2ca20d2"
	}
}, {
	"id": 33,
	"name": "Лемур",
	"class": "maroder",

	"health": 90,
	"max_health": 100,
	"energy": 80,
	"max_energy": 90,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,

	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 3 }],

	"side": 2,

	"_auth": {
		"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
		"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
	}
}/*, {
	"id": 40,
	"name": "Гандольери",
	"class": "maroder",

	"health": 40,
	"max_health": 120,
	"energy": 100,
	"max_energy": 100,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,
	
	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 1 }],

	"side": 1,

	"_auth": {
		"token": "34_a39fc96f-8914-4fbd-a709-005bf7ba2c0a",
		"session": "cbdf4a22-06e2-4a5b-b70c-b1bac2ca20d2"
	}
}, {
	"id": 41,
	"name": "Фауст",
	"class": "maroder",

	"health": 90,
	"max_health": 100,
	"energy": 80,
	"max_energy": 90,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,

	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 3 }],

	"side": 2,

	"_auth": {
		"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
		"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
	}
}, {
	"id": 42,
	"name": "Цваргершейтн",
	"class": "maroder",

	"health": 40,
	"max_health": 120,
	"energy": 100,
	"max_energy": 100,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,
	
	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 1 }],

	"side": 1,

	"_auth": {
		"token": "34_a39fc96f-8914-4fbd-a709-005bf7ba2c0a",
		"session": "cbdf4a22-06e2-4a5b-b70c-b1bac2ca20d2"
	}
}, {
	"id": 43,
	"name": "Невыносимый лось",
	"class": "maroder",

	"health": 90,
	"max_health": 100,
	"energy": 80,
	"max_energy": 90,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,

	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 3 }],

	"side": 2,

	"_auth": {
		"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
		"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
	}
},{
	"id": 51,
	"name": "Нихтферштейн",
	"class": "maroder",

	"health": 90,
	"max_health": 100,
	"energy": 80,
	"max_energy": 90,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,

	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 3 }],

	"side": 3,

	"_auth": {
		"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
		"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
	}
}, {
	"id": 52,
	"name": "Вариматрас",
	"class": "maroder",

	"health": 40,
	"max_health": 120,
	"energy": 100,
	"max_energy": 100,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,
	
	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 1 }],

	"side": 3,

	"_auth": {
		"token": "34_a39fc96f-8914-4fbd-a709-005bf7ba2c0a",
		"session": "cbdf4a22-06e2-4a5b-b70c-b1bac2ca20d2"
	}
}, {
	"id": 53,
	"name": "Ямарад",
	"class": "maroder",

	"health": 90,
	"max_health": 100,
	"energy": 80,
	"max_energy": 90,

	"strength": 10,
	"agility": 10,
	"intelligence": 10,
	"endurance": 10,
	"luck": 10,

	"active_skills": [{ id: 1, level: 1 }, { id: 2, level: 2 }],
	"passive_skills": [{ id: 1, level: 3 }],

	"side": 3,

	"_auth": {
		"token": "33_61abae88-b55e-487d-bba3-88f31f3b7ade",
		"session": "213a186b-8e5e-403e-8b1a-ae7e0575d99c"
	}
}*/];

context.battle = new bc.Battle({
	"id": "737820fe-989f-4a10-a330-ca8f2e6cc397",
	"status": 2,
	"heroes": context.heroes
});

require('base_server')('BATTLE', 7777, require('./battle_serverIO.js'), require('./battle_clientIO.js'), require('./config.local.json'), context, false);