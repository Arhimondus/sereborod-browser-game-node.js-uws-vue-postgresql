const getPort = require('get-port'),
	CircularJSON = require('circular-json'),
	spawn = require('child_process').spawn;

serverIO = {
	['hero/assign']({ hero, token }, { heroes }) {
		if (heroes.findIndex((h) => h.id === hero.id) < 0) {
			heroes.push(Object.assign(hero, { _auth: { token } }));
		}
		return 'okay hero/assign';
	},
	['hero/list'](_, { heroes }) {
		console.log('ARENA hero/list try');
		return 'list::' + CircularJSON.stringify(heroes);
	},
	['battle/list'](_, { heroes }) {
		console.log('ARENA hero/list try');
		return 'list::' + CircularJSON.stringify(heroes);
	},
	async ['new'](body, { battles, servers }) {	
		let heroes = await servers.person('heroes/multiple', body.heroes.map(m => m.id));
		heroes.forEach((hero, index) => {
			hero.side = index % 2 == 0 ? 1 : 2;
			let { token, session } = body.heroes.find(h => h.id == hero.id)._auth;
			hero._auth = { token, session };
		});
		body.heroes = heroes;

		console.log(CircularJSON.stringify(body));

		let data = Buffer.from(CircularJSON.stringify(body)).toString('base64'); // конвертируем данные в base64
		let port = await getPort();

		let process = spawn('nodemon', ['--inspect=7777', 'battle', `--port=${port}`, `--data=${data}`], { shell: true, detached: true }); // передача параметров в командной строке для запуска battle_server
		process.unref();
		
		battles.push({ port, process });
		
		console.log('new battle at port ' + port);
		return port;
	}
};

module.exports = serverIO;