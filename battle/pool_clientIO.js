clientIO = {
	async ['game/data']() {
		var hero_id = socket.hero_id;
		var data = await db.one(`SELECT data FROM battles WHERE status = 2 AND heroes @> '[{"id": $<hero_id>}]' RETURNING data`, {
			hero_id,
		});
		fn(data);
	},
	['game/slot/set'](slot) {
		db.query(`UPDATE battles SET data = data || '[{"id":$<id^>, "name":"$<name^>"}]'::jsonb WHERE id = $<battle_id>`,{
			id: hero.id,
			name: hero.name,
			battle_id: battle.id,
		})
	},
	async ['hero/current_battle'](fn) {
		var hero_id = socket.hero_id;
		var battle = await db.oneOrNone(`SELECT id, status FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<hero_id>}]'`, {
			hero_id,
		});
		fn(battle);
	},
	async ['battle/create']() {
		console.log('battle/create-emit');
		try {
			var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
				id: socket.hero_id
			});
			if(!hero)
				throw new Error("Неверный герой?");
			
			var battles = await db.query(`SELECT id FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]'`, {
				id: hero.id,
			});
			if(battles.length > 0)
				throw new Error("Герой уже в активной заявке!");

			var battle = { status: 1 };
			battle.heroes = [{ id: hero.id, name: hero.name }];
			battle.id = (await db.one('INSERT INTO battles(mode, params, heroes) VALUES(${mode}, ${params}, ${heroes}) RETURNING id', {
				mode: battle.mode,
				params: battle.params,
				heroes: JSON.stringify(battle.heroes),
			})).id;
			socket.emit('battle/create/success', battle);
			socket.broadcast.emit('battle/create', battle);
			//io.emit('battle/create', battle);
		} catch({ message }) {
			socket.emit('battle/create/error', message);
			console.log("ERROR battle/create " + message);
		}
	},
	async ['battle/join']({ battle_id }) {
		try {
			var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
				id: socket.hero_id
			});
			if(!hero)
				throw new Error("Неверный герой?");
			var battles = await db.query(`SELECT id FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]'`, {
				id: hero.id,
			});
			if(battles.length > 0)
				throw new Error("Герой уже присутствует в другом бою!");
			var battle = await db.one('SELECT * FROM battles WHERE id = ${battle_id} AND status = 1 LIMIT 1', {					
				battle_id
			});
			var mode = sereborod.Modes[battle.mode];
			if(!mode.check_requirements(hero, battle)) //Включая вместимость - здесь?
				throw new Error("Герой не подходит под требования мода(режима)!");	
			battle.heroes.push({ id: hero.id, name: hero.name });
			//ДАЛЕЕ КРАЙНЕ ВАЖНА КРЫШЕЧКА!!! Она необработанные переменные вставляет. Узнать почему не работает без неё!
			if(mode.is_mode_ready(battle)) battle.status = 2;
			await db.query(`UPDATE battles SET status = $<status>, heroes = heroes || '[{"id":$<id^>, "name":"$<name^>"}]'::jsonb WHERE id = $<battle_id>`,{
				status: battle.status,
				id: hero.id,
				name: hero.name,
				battle_id: battle.id,
			});
			
			socket.emit('battle/join/success', battle);
			socket.broadcast.emit('battle/change', battle);
		} catch({ message }) {
			socket.emit('battle/join/error', message);
			console.log("ERROR battle/create " + message);
		}
	},
	async ['battle/leave']({ battle_id }) {
		try {
			var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
				id: socket.hero_id
			});
			if(!hero)
				throw new Error("Неверный герой?");
			var battles = await db.query(`SELECT * FROM battles WHERE id = $<battle_id> AND status = 1 AND heroes @> '[{"id": $<id>}]' LIMIT 1`, {					
				battle_id: battle_id,
				id: hero.id,
			});
			if(battles.length == 0)
				throw new Error("Герой пытается покинуть бой, в котором его нету!");
			var battle = battles[0];
			battle.heroes = battle.heroes.filter((f) => f.id != hero.id);
			if(battle.heroes.length > 0) {
				await db.query('UPDATE battles SET heroes = ${heroes} WHERE id = $<battle_id>', {
					heroes: JSON.stringify(battle.heroes),
					battle_id: battle.id,
				});
				socket.broadcast.emit('battle/change', battle);
			}
			else {
				await db.query('DELETE FROM battles WHERE id = $<battle_id>', {
					battle_id: battle.id
				});
				io.emit('battle/remove', battle);
			}
			socket.emit('battle/leave/success', battle);
		} catch({ message }) {
			socket.emit('battle/leave/error', message);
			console.log("ERROR battle/leave " + message);
		}
	},
};

module.exports = clientIO;