const bc = require('../battle_common/common.js');

clientIO = {
	async ['set_slots']({ data, hero }, { battle, io }) {
		let h = battle.heroes.find(h => h._chero == hero);
		if(!h.alive) throw new Error('Герой мёртв');

		h.slots = data;
		
		if(battle.heroes_alive.every(h => h.slots != null)) {
			battle.do_all_actions();
			io.broadcast('battle/next/round', battle.serialize());
			return 'ok_last';
		}
		return 'ok';
	},
	async ['get_battle']({ hero }, { battle, io }) {
		return battle.serialize();
	},
};

module.exports = clientIO;