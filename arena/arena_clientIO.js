const uuid = require('uuid');

clientIO = {
	async ['battle/status/open']({ hero }, { battles }) {
		return battles.filter(f => f.status == 'open');
	},
	async ['hero/battle/current_id']({ hero }, { battles }) {
		let battle = battles.find(b => b.heroes.includes(hero));
		if(battle) return battle.id;
		else return null;
	},
	async ['battle/create']({ hero, socket }, { battles, io }) {
		if (battles.some(b => b.heroes.includes(hero)))
			throw new Error('Персонаж уже в бою.');

		let battle = {
			status: 'open',
			heroes: [hero],
			id: uuid(),
		};
		battles.push(battle);

		socket.broadcast('battle/create', battle);
		return battle;
	},
	async ['battle/join']({ hero, data, socket }, { battles, servers, io }) {
		if (battles.some(b => b.heroes.includes(hero)))
			throw new Error('Герой уже присутствует в другом бою!');

		let battle_id = data.id;

		let battle = battles.find(b => b.id === battle_id);
		if (!battle)
			throw new Error('Бой не найдён!');
		if (battle.heroes.length != 1)
			throw new Error('Невозможно присоединиться');

		battle.heroes.push(hero);
		battle.status = 'active';

		let port = await servers.battle('new', battle);
		battle.port = port;
		
		socket.broadcast('battle/change', battle);
		return battle;
	},
	async ['battle/leave']({ hero, battle_id }, { battles, io }) {
		let battle = battles.find(b => b.heroes.includes(hero));
		if(!battle)
			throw new Error('Герой пытается покинуть бой, в котором его нет!');
		
		battle.heroes.splice(battle.heroes.indexOf(hero), 1);
		
		if(battle.heroes.length > 0)
			io.broadcast('battle/change', battle);
		else {
			battles.splice(battles.indexOf(battle), 1);
			io.broadcast('battle/remove', battle);
		}
		
		return 'ok';
	},
};

module.exports = clientIO;