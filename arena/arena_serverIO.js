serverIO = {
	async ['hero/assign']({ hero, token }, { heroes }) {
		let finded_hero = heroes.find((h) => h.id === hero.id);
		if(!finded_hero)
			heroes.push({
				id: hero.id,
				name: hero.name,
				level: hero.level,
				_auth: { token },
			});
		else 
			finded_hero._auth.token = token;
		return 'ok';
	},
	async ['hero/list'](_, { heroes }) {
		console.log('ARENA hero/list try');
		return heroes;
	},
};

module.exports = serverIO;